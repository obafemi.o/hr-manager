defmodule Billout.Authorization do
  import Ecto.Query, warn: false
  alias Billout.Repo
  alias Billout.Authorization.{Permission, Role}

  defdelegate authorize(action, user, params), to: Billout.Policy

  def create_role(attrs \\ %{}) do
    %Role{}
    |> Role.changeset(attrs)
    |> Repo.insert()
  end

   def list_role() do
    Repo.all(Role) |> Repo.preload(:permissions)
  end

  def list_permission() do
    Repo.all(Permission)
  end

  def create_permission(attrs \\ %{}) do
    %Permission{}
    |> Permission.changeset(attrs)
    |> Repo.insert()
  end

  def show_array() do
   arrays= show_permission
   |> Enum.map( fn array-> handle_array(array) end)

  end

  def handle_array(array) do
    cond do
     array -> array.role
     array -> array.action
    end
  end
 

  def show_permission() do
    action = "create"
    Permission
    |> where([p], p.action == ^action)
    |> join(:left, [p], r in Role, on: p.role_id == r.id)
    |> select([p, r], %{role: r.name, action: p.action})
    |> Repo.one()
  end

   
  def create_multiple_permissions(role_id, attrs \\ %{}) do
    Enum.map(attrs, fn attr -> handle_create_permissions(attr, role_id) end)
  end

  def handle_create_permissions(attr, role_id) do       
    role = Repo.get!(Role, role_id)
    Ecto.build_assoc(role, :permissions)
    |> Permission.changeset(%{action: attr})
    |> Repo.insert()
  end

  def upsert_permissions(role, attrs ) do
    Enum.map(attrs, fn attr-> handle_upsert(attr, role) end)
  end

  def handle_upsert(attr, role ) do     
    if attr !==" " do
      permission= Repo.get_by(Permission, name: attr )  
      if is_nil(permission) == false do           
        permission
        |> Permission.changeset(%{action: attr})
        |> Repo.update
      end

      if is_nil(permission) == true do           
        role_id  = Map.get(role, "id" )        
        role = Repo.get!(Role, role_id  )
        Ecto.build_assoc(role, :permissions)
        |> Permission.changeset(%{action: attr})
        |> Repo.insert
      end
    end
  end

  def change_multiple_employee_roles(role_id, attrs \\ %{}) do
    Enum.map(attrs, fn attr -> handle_create_permissions(attr, role_id) end)
  end
end
