defmodule Billout.Authorization.Role do
  use Ecto.Schema
  @derive {Jason.Encoder, only: [:id, :name, :permissions]}
  import Ecto.Changeset
  alias Billout.Authorization.Permission

  schema "roles" do
    field :name, :string
    has_many :permissions, Permission

    timestamps()
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
