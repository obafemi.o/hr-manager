defmodule Billout.Authorization.Permission do
  use Ecto.Schema
  @derive {Jason.Encoder, only: [:id, :name, :action, :role_id]}
  import Ecto.Changeset
  alias Billout.Authorization.Role

  schema "permissions" do
    field :name, :string
    field :action, :string
    belongs_to :role, Role

    timestamps()
  end

  @doc false
  def changeset(permission, attrs) do
    permission
    |> cast(attrs, [:name, :action, :role_id])
    |> validate_required([ :action])
  end
end
