defmodule Billout.Payroll do
  import Ecto.Query, warn: false
  alias Billout.Repo

  alias Billout.Organization
  alias Billout.Organization.Employee
  alias Billout.Payroll.{Ledger, Payrun}


  def get_current_payroll() do
    q = from l in Ledger, select: max(l.value)
    Repo.all(q)
  end

  def create_ledger(attrs \\ %{}) do
    parse_earnings(attrs)
    parse_deductions(attrs)
  end

  defp parse_earnings(attrs) do
    filter = "Monthly"
    earnings = Organization.get_employees_earnings_by(filter)
    Enum.all?(earnings, fn e ->
      ledger = %{
        "value" => Decimal.mult(e.rate, 1),
        "description" => e.type,
        "payrun_id" => attrs["payrun_id"],
        "start_date" => attrs["start_date"],
        "end_date" => attrs["end_date"],
        "employee_id" => e.employee_id,
        "earnings" => Decimal.mult(e.rate, 1)
      }

      case insert_to_ledger(ledger) do
        {:ok, _} ->
          true
        {:error, _} ->
          false
      end
    end)

  end

  defp parse_deductions(attrs) do
    filter = "Monthly"
    filter
    |> Organization.get_employees_deductions_by()
    |> Enum.all?(fn e ->
      ledger = %{
        "value" => Decimal.mult(e.rate, -1),
        "description" => e.type,
        "payrun_id" => attrs["payrun_id"],
        "start_date" => attrs["start_date"],
        "end_date" => attrs["end_date"],
        "employee_id" => e.employee_id,
        "deductions" => Decimal.mult(e.rate, -1),
      }

      case insert_to_ledger(ledger) do
        {:ok, _} ->
          true
        {:error, _} ->
          false
      end
    end)
  end

  def insert_to_ledger(attrs \\ %{}) do
    %Ledger{}
    |> Ledger.changeset(attrs)
    |> Repo.insert()
  end

  def update_ledger(%Ledger{} = ledger, attrs \\ %{}) do
    ledger
    |> Ledger.changeset(attrs)
    |> Repo.update()
  end

  def update_ledger_by_id(ledger_id, attrs \\ %{}) do
    ledger = Repo.get!(Ledger , ledger_id)
    ledger
    |> Ledger.changeset(attrs)
    |> Repo.update()
  end

  def delete_from_ledger(%Ledger{} = ledger) do
    Repo.delete(ledger)
  end

  def get_max_payroll() do
    max_payroll = Repo.one (from l in "ledgers", select: max(l.payrun_id));
    if is_nil(max_payroll) do
      max_payroll = 01
      list_payroll(max_payroll)
    else
      list_payroll(max_payroll)
    end
  end

  def list_payroll(max_payroll) do
    Ledger
    |> where([l], l.payrun_id == ^max_payroll)
    |> join(:left, [l], e in Employee, on: l.employee_id == e.id)
    |> group_by([l, e], [l.employee_id, l.payrun_id, e.first_name, e.surname])
    |> select([l, e], %{employee_id: l.employee_id, payrun_id: l.payrun_id, deductions: fragment("abs(?)", (filter(sum(l.value),l.value < 0))), earnings: fragment("abs(?)", (filter(sum(l.value),l.value > 0))), net_pay: sum(l.value), employee_name: fragment("concat(?, ' ', ?)", e.first_name, e.surname)})
    |> order_by([l, e], asc: e.first_name, asc: e.surname)
    |> Repo.all()
  end

  def list_previous_payroll(payrun_id) do
    Ledger
    |> where([l], l.payrun_id == ^payrun_id)
    |> join(:left, [l], e in Employee, on: l.employee_id == e.id)
    |> group_by([l, e], [l.employee_id, l.payrun_id, e.first_name, e.surname])
    |> select([l, e], %{employee_id: l.employee_id, payrun_id: l.payrun_id, deductions: fragment("abs(?)", (filter(sum(l.value),l.value < 0))), earnings: fragment("abs(?)", (filter(sum(l.value),l.value > 0))), net_pay: sum(l.value), employee_name: fragment("concat(?, ' ', ?)", e.first_name, e.surname)})
    |> order_by([l, e], asc: e.first_name, asc: e.surname)
    |> Repo.all()
  end

  def get_ledger_total() do
    max_payroll = Repo.one (from l in "ledgers", select: max(l.payrun_id));
    if is_nil(max_payroll) do
      max_payroll = 01
      list_total_payroll(max_payroll)
      |>  Enum.count(&(&1))
    else
      list_total_payroll(max_payroll)
      |>  Enum.count(&(&1))
    end

  end

  def list_total_payroll(max_payroll) do
    Ledger
    |> where([l], l.payrun_id == ^max_payroll)
    |> join(:left, [l], e in Employee, on: l.employee_id == e.id)
    |> group_by([l, e], [l.employee_id, l.payrun_id, e.first_name, e.surname])
    |> select([l, e], %{employee_id: l.employee_id, payrun_id: l.payrun_id, deductions: fragment("abs(?)", (filter(sum(l.value),l.value < 0))), earnings: fragment("abs(?)", (filter(sum(l.value),l.value > 0))), net_pay: sum(l.value), employee_name: fragment("concat(?, ' ', ?)", e.first_name, e.surname)})
    |> order_by([l, e], asc: e.first_name, asc: e.surname)
    |> Repo.all()
  end

  def get_employee_payroll(employee_id, start_date, end_date) do
    Ledger
    |> where([l], l.employee_id == ^employee_id and l.start_date >= ^start_date and l.end_date <= ^end_date)
    |> Repo.all()
  end

  def list_employee_payroll(id) do

    Ledger
    |> where([l], l.employee_id == ^id)
    |> join(:left, [l], e in Employee, on: l.employee_id == e.id)
    |> group_by([l, e], [l.employee_id, l.payrun_id, e.first_name, e.surname])
    |> select([l, e], %{employee_id: l.employee_id, payrun_id: l.payrun_id, deductions: fragment("abs(?)", (filter(sum(l.value),l.value < 0))), earnings: fragment("abs(?)", (filter(sum(l.value),l.value > 0))), net_pay: sum(l.value), employee_name: fragment("concat(?, ' ', ?)", e.first_name, e.surname)})
    |> order_by([l, e], asc: e.first_name, asc: e.surname)
    |> Repo.all()
  end

  # def previous_payroll(start_date, end_date) do
  #   Ledger
  #   |> where([l], l.start_date >= ^start_date and l.end_date <= ^end_date)
  #   |> Repo.all()
  # end

  def previous_payrolls(start_date, end_date) do
    Payrun
    |> where([p], p.start_date >= ^start_date and p.end_date <= ^end_date)
    |> Repo.all()
    |> Repo.preload(:ledgers)
  end

  def create_payrun(attrs \\ %{}) do
    %Payrun{}
    |> Payrun.changeset(attrs)
    |> Repo.insert()
  end



  def list_payruns() do
    Payrun
    |> Repo.all()
    |> Repo.preload(:ledgers)
  end


  def get_payrun!(id), do: Repo.get!(Payrun, id) |> Repo.preload(:ledgers)
end
