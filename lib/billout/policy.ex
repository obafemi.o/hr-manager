defmodule Billout.Policy do
  import Ecto.Query, warn: false
  alias Billout.Repo
  alias Billout.Users.User
  alias Billout.Authorization.Role
  alias Billout.Authorization.Permission
  @behaviour Bodyguard.Policy

  # Admin users can do anything
  def authorize(_, %User{role: "HR"}, _), do: true


  def authorize(action, user, _) do
    permissions = show_permission(action)
    cond do
      user.role == permissions.role -> :ok
      true -> :error
    end
  end

  defp show_permission(action) do
    action_to_string = Atom.to_string(action)
    Permission
    |> where([p], p.action == ^action_to_string )
    |> join(:left, [p], r in Role, on: p.role_id == r.id)
    |> select([p, r], %{role: r.name, action: p.action})
    |> Repo.one()
  end

# Catch-all: deny everything else
  def authorize(_, _, _), do: false
end
