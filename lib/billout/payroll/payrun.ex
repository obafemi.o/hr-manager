defmodule Billout.Payroll.Payrun do
  use Ecto.Schema
  import Ecto.Changeset
  alias Billout.Payroll.Ledger
  @derive {Jason.Encoder, only: [:id, :start_date, :end_date, :pay_type, :employee_type]}

  schema "payruns" do
    field :employee_type, :string
    field :end_date, :date
    field :pay_type, :string
    field :start_date, :date
    has_many :ledgers, Ledger
    timestamps()
  end

  @doc false
  def changeset(payrun, attrs) do
    payrun
    |> cast(attrs, [:start_date, :end_date, :pay_type, :employee_type])
    |> validate_required([:start_date, :end_date])
  end
end
