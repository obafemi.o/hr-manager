defmodule Billout.Payroll.Ledger do
  use Ecto.Schema
  import Ecto.Changeset
  alias Billout.Organization.{Employee}
  alias Billout.Payroll.Payrun
  # @derive {Jason.Encoder, only: [:id, :start_date, :end_date, :value, :description, :earnings, :deductions, :processed, :payrun_id, :employee_id]}

  schema "ledgers" do
    field :description, :string
    field :end_date, :date
    # field :payrun_id, :decimal
    field :start_date, :date
    field :value, :decimal
    field :earnings, :decimal
    field :deductions, :decimal
    field :processed, :boolean
    belongs_to :employee, Employee
    belongs_to :payrun, Payrun

    timestamps()
  end

  @doc false
  def changeset(ledger, attrs) do
    ledger
    |> cast(attrs, [:value, :description, :payrun_id, :start_date, :end_date, :employee_id, :earnings, :deductions, :processed, :payrun_id ])
    |> validate_required([:value, :description, :payrun_id, :employee_id])
    |> foreign_key_constraint(:payrun_id)
  end
end
