defmodule Billout.Organization.Package do
  use Ecto.Schema
  import Ecto.Changeset
  @derive {Jason.Encoder, only: [:id, :tag, :description]}
  alias Billout.Organization.{Employee, EmployeePackage, Earning, Deduction}

  schema "packages" do
    field :tag, :string
    field :description, :string
    has_many :earnings, Earning
    has_many :deductions, Deduction

    many_to_many(:employees, Employee, join_through: EmployeePackage, on_replace: :delete)
    timestamps()
  end

  @doc false
  def changeset(package, attrs) do
    package
    |> cast(attrs, [:tag, :description])
    |> validate_required([])
  end
end
