defmodule Billout.Organization.Employee do
  use Ecto.Schema
  import Ecto.Changeset
  @derive {Jason.Encoder, only: [:id, :title, :first_name, :surname, :middle_name, :phone, :email, :marital_status, :dob, :state, :country, :children, :dependents, :address,  :address_2, :state, :role, :gender, :status, :account_name, :account_number, :bank_name, :branch_address, :sort_code, :bank_country, :position, :is_user]}
  alias Billout.Organization.{Employee, Package, EmployeePackage}
  alias Billout.Payroll.Ledger
  import Ecto.Query, only: [from: 2]


  schema "employees" do
    field :address, :string
    field :address_2, :string
    field :children, :integer
    field :country, :string
    field :dependents, :integer
    field :dob, :date
    field :email, :string
    field :first_name, :string
    field :gender, :string
    field :marital_status, :string
    field :middle_name, :string
    field :phone, :string
    field :role, :string
    field :position, :string
    field :state, :string
    field :surname, :string
    field :title, :string
    field :status, :string
    field :account_name, :string
    field :account_number, :string
    field :bank_name, :string
    field :branch_address, :string
    field :sort_code, :string
    field :bank_country, :string
    field :is_user, :boolean
    has_many :ledgers, Ledger
    many_to_many(:packages, Package, join_through: EmployeePackage, on_replace: :delete)

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:title, :gender, :role, :first_name, :surname, :middle_name, :email, :country, :state, :phone, :address, :address_2, :dob, :marital_status, :children, :dependents, :status, :account_name, :account_number, :bank_name, :branch_address, :sort_code, :bank_country, :position, :is_user ])
    |> validate_required([:title, :gender, :role, :first_name, :surname, :email, :country, :state, :phone, :address, :dob, :marital_status, :children, :dependents, :status])
  end

  def role_changeset(employee, attrs) do
    employee
    |> cast(attrs, [ :role])
    |> validate_required([:role])
  end

  @spec search(any, any) :: Ecto.Query.t()
  def search(query, search_term) do
    wildcard_search = "%#{search_term}%"

    from employee in query,
      where: ilike(employee.first_name, ^wildcard_search),
      or_where: ilike(employee.surname, ^wildcard_search)
  end
end
