defmodule Billout.Organization.Deduction do
  use Ecto.Schema
  import Ecto.Changeset

  alias Billout.Organization.Package

  schema "deductions" do
    field :basis, :string
    field :end_date, :date
    field :frequency, :string
    field :rate, :decimal
    field :start_date, :date
    field :type, :string
    belongs_to :package, Package

    timestamps()
  end

  @doc false
  def changeset(deduction, attrs) do
    deduction
    |> cast(attrs, [:basis, :frequency, :rate, :type, :package_id, :start_date, :end_date])
    |> validate_required([:basis, :frequency, :rate, :type])
  end
end
