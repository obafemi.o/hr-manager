defmodule Billout.Users.User do
  use Ecto.Schema
  import Ecto.Changeset
  use Pow.Ecto.Schema
  alias Billout.Organization.Employee
  use Pow.Extension.Ecto.Schema,
    extensions: [PowResetPassword, PowEmailConfirmation]


  schema "users" do
    pow_user_fields()
    field :role, :string, default: "Employee"
    field :is_first_time, :boolean
    belongs_to :employee, Employee

    timestamps()
  end

  def changeset(user, attrs) do
    user
    |> pow_changeset(attrs)
    |> cast(attrs, [:employee_id, :role, :is_first_time])
    |> validate_required([:is_first_time])
  end

  def reset_password_changeset(user, attrs) do
    user
    |> pow_password_changeset(attrs)
    |> cast(attrs, [:is_first_time])
    |> validate_required([:is_first_time, :password])

  end

  def role_changeset(user, attrs) do
    user 
    |> cast(attrs, [ :role])
    |> validate_required([:role])
  end
end
