defmodule Billout.Organization do
  import Ecto.Query, warn: false
  alias Billout.Repo
  alias Billout.Organization.{Employee, Package, EmployeePackage, Earning, Deduction}
  alias Billout.Users.User
  defdelegate authorize(action, user, params), to: Billout.Policy


  def list_employees() do
    Repo.all(Employee)
  end

  def list_employees(offset, limit) do
    Employee
    |> order_by(asc: :first_name, asc: :surname)
    |> limit(^limit)
    |> offset(^offset)
    |> Repo.all()
    |> Repo.preload(:packages)
  end

  def get_total() do
    Repo.aggregate(Employee, :count, :id)
  end

  def search(params) do
    search_term = get_in(params, ["query"])

    Employee
    |> Employee.search(search_term)
    |> Repo.all()
  end

  def list_employees_desc() do
    Employee |> order_by(desc: :first_name) |> Repo.all()
  end

  def get_employee!(id), do: Repo.get!(Employee, id) |> Repo.preload(:packages)

  def create_employee(attrs \\ %{}) do
    %Employee{}
    |> Employee.changeset(attrs)
    |> Repo.insert()
  end

  def is_file_data_valid?(attrs \\ %{}) do
    changeset =
      %Employee{}
      |> Employee.changeset(attrs)

    changeset.valid?
  end

  def update_employee(%Employee{} = employee, attrs) do
    employee
    |> Employee.changeset(attrs)
    |> Repo.update()
  end

  def delete_employee(%Employee{} = employee) do
    Repo.delete(employee)
  end

  def change_employee(%Employee{} = employee) do
    Employee.changeset(employee, %{})
  end

  # def create_package(attrs \\ %{}) do
  #   %Package{}
  #  |> Ecto.Changeset.change()
  #  |> Ecto.Changeset.put_embed(:earning, attrs )
  #  |> Repo.insert()
  # end

  # def save_package(attrs \\ %{}) do
  #   earnings = Map.get(attrs, :earnings)
  #   %Package{}
  #   |> Ecto.Changeset.change
  #   |> Ecto.Changeset.put_embed(:earnings, earnings)
  #   |> Repo.insert()

  # end

  def list_packages do
    Package
    |> where([p],is_nil(p.tag )== false )
    |> Repo.all()
  end

  def list_package_templates(offset) do
    Package
    |> where([p], is_nil(p.tag) == false)
    |> order_by(asc: :tag)
    |> limit(25)
    |> offset(^offset)
    |> Repo.all()
    |> Repo.preload(:earnings)
  end

  def get_package_total() do
    query = from p in "packages", where: is_nil(p.tag) == false
    Repo.aggregate(query, :count, :tag)
  end

  # def list_packages do
  #   Repo.all(Package)
  # end
  def get_package!(id), do: Repo.get!(Package, id) |> Repo.preload(:earnings)

  # def list_packages do
  #   Package
  #   |> where([p],is_nil(p.tag )== false )
  #   |> Repo.all
  # end
  # def get_package!(id), do: Repo.get!(Package, id) #|> Repo.preload(:employees)

  def create_package(attrs \\ %{}) do
    %Package{}
    |> Package.changeset(attrs)
    |> Repo.insert()
  end

  def link_employee_package(employee_id, package_attrs \\ %{}) do
    employee = Repo.get!(Employee, employee_id)
    {:ok, package} = create_package(package_attrs)

    package
    |> Repo.preload(:employees)
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:employees, [employee])
    |> Repo.update()
  end

  def create_employee_package(employee_id, package_id) do
    employee = Repo.get!(Employee, employee_id)
    package = Repo.get!(Package, package_id)
    # {:ok, package} = create_package(package_attrs)

    package
    |> Repo.preload(:employees)
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:employees, [employee])
    |> Repo.update()
  end



  def update_package(%Package{} = package, attrs) do
    package
    |> Package.changeset(attrs)
    |> Repo.update()
  end

  # def create_employee_package(attrs \\ %{}) do
  #   IO.inspect attrs
  #   %EmployeePackage{}
  #   |> EmployeePackage.changeset(attrs)
  #   |> Repo.insert()
  # end

  def delete_package(%Package{} = package) do
    Repo.delete(package)
  end

  def change_package(%Package{} = package) do
    Package.changeset(package, %{})
  end

  def assign_package(pid, eid, sd, ed) do
    EmployeePackage.changeset(%EmployeePackage{}, %{
      package_id: pid,
      employee_id: eid,
      start_date: sd,
      end_date: ed
    })
    |> Repo.insert()
  end

  def get_employee_package(employee_id) do

    # earnings = from e in Earning, where: e.startDate >= ^start_date and e.endDate <= ^end_date
    # deductions = from d in Deduction, where: d.start_date >= ^start_date and d.end_date <= ^end_date

    EmployeePackage
    |> where([ep], ep.employee_id == ^employee_id)
    |> preload([package: :earnings, package: :deductions])
    |> last(:inserted_at)
    |> Repo.one()


    # EmployeePackage
    # |> where([ep], ep.employee_id == ^employee_id)
    # |> join(:left, [ep], _ in assoc(ep, :package))
    # |> join(:left, [_, p], _ in assoc(p, :earnings))
    # |> join(:left, [_, p], _ in assoc(p, :deductions))
    # |> preload([_, p, _, _], [package: {p, earnings: ^earnings, deductions: ^deductions}])
    # |> last(:inserted_at)
    # |> Repo.one()
  end

  def get_all_employee_package!(employee_id) do

    EmployeePackage
    |> where([ep], ep.employee_id == ^employee_id)
    |> preload([package: :earnings])
    |> Repo.all()
  end

  def change_employee_role(attrs \\ %{}, config) do
    email = Map.get(attrs, "email" )
    User
    |> where([u], u.email == ^email )
    |> Repo.one()
    |> User.role_changeset(attrs)
    |> Pow.Ecto.Context.do_update(config)
  end

  def change_multiple_employee_roles(employee_params, attrs \\ %{}, config) do
    Enum.map(employee_params, fn employee_param -> change_single_employee_role(employee_param, attrs, config) end)
  end

  def change_single_employee_role(email, attrs, config) do
    role = Map.get(attrs, "name" )
    is_user = check_if_employee_is_a_user(email)
    if (is_user !== nil) do
       # Update role on User Changeset
      is_user
      |> User.role_changeset(%{role: role})
      |> Pow.Ecto.Context.do_update(config)
      # Update role on Employee Changeset
      Employee
      |> where([u], u.email == ^email )
      |> Repo.one()
      |> Employee.role_changeset(%{role: role})
      |> Repo.update()
    end
  end

  def check_if_employee_is_a_user(email) do
    User
    |> where([u], u.email == ^email )
    |> Repo.one()
  end


  # ----------------------- Start of Earnings ---------------------------- #
  def get_employees_earnings_by(param) do
    query = from ep in EmployeePackage,
        join: p in assoc(ep, :package),
        join: e in assoc(p, :earnings),
        where: e.frequency == ^param,
        select: %{employee_id: ep.employee_id, type: e.type, rate: e.rate, frequency: e.frequency, basis: e.basis}

    Repo.all(query)
  end

  def create_earning(attrs \\ %{}) do
    %Earning{}
    |> Earning.changeset(attrs)
    |> Repo.insert()
  end

  def create_earnings(package_id, attrs \\ %{}) do
    Enum.map(attrs, fn attr -> handle_create(attr, package_id) end)
  end

  def handle_create(attr, package_id) do
    startDate = Map.get(attr, "startDate" )
    endDate = Map.get(attr, "endDate" )
    attr= Map.put(attr, "startDate", Date.from_iso8601!(startDate))
    attr= Map.put(attr, "endDate", Date.from_iso8601!(endDate))
    IO.inspect attr
    package = Repo.get!(Package, package_id)
    Ecto.build_assoc(package, :earnings)
    |> Earning.changeset(attr)
    |> Repo.insert()
  end

  def upsert_earnings(package_id, attrs \\ %{}) do
    Enum.map(attrs, fn attr-> handle_upsert(attr, package_id) end)
  end

  def handle_upsert(attr, package_id ) do
    id = Map.get(attr, "id" )
    if is_nil(id)== false do
      Repo.get!(Earning, id )
      |> Earning.changeset(attr)
      |> Repo.update
    end

    if is_nil(id)== true do
      package = Repo.get!(Package, package_id )
      Ecto.build_assoc(package, :earnings)
      |> Earning.changeset(attr)
      |> Repo.insert
    end

  end

   def get_earnings!(package_id) do
    q = from Earning, where: [package_id: ^package_id]
    Repo.all(q)
  end
  # ------------------------ End of Earnings ----------------------------- #

  # ---------------------- Start of Deductions --------------------------- #
  def create_deduction(package_id, attrs \\ %{}) do
    package = Repo.get!(Package, package_id)
    gross_pay = calculate_gross_pay(attrs)
    hmo = calculate_hmo()
    tax = calculate_tax(gross_pay)
    pension = calculate_pension(gross_pay)

    # insert_deduction(%{"basis" => "Monthly", "frequency" => "Monthly","rate" => hmo,"type" => "HMO"}, package)
    # insert_deduction(%{"basis" => "Monthly", "frequency" => "Monthly","rate" => tax,"type" => "Tax"}, package)
    # insert_deduction(%{"basis" => "Monthly", "frequency" => "Monthly","rate" => pension,"type" => "Pension"}, package)
  end

  defp insert_deduction(attr, package) do
    Ecto.build_assoc(package, :deductions)
    |> Deduction.changeset(attr)
    |> Repo.insert()
  end

  def create_deduction_record(attrs \\ %{}) do
    %Deduction{}
    |> Deduction.changeset(attrs)
    |> Repo.insert()
  end

  def get_employees_deductions_by(param) do
    query = from ep in EmployeePackage,
        join: p in assoc(ep, :package),
        join: d in assoc(p, :deductions),
        where: d.frequency == ^param,
        select: %{employee_id: ep.employee_id, type: d.type, rate: d.rate, frequency: d.frequency, basis: d.basis}

    Repo.all(query)
  end

  def get_deductions!(package_id) do
    q = from Deduction, where: [package_id: ^package_id]
    Repo.all(q)
  end

  def calculate_hmo() do
    hmo = 2500
    hmo
  end

  defp calculate_tax(gross_pay) do
    percentage = Decimal.from_float(0.07)
    Decimal.mult(Decimal.from_float(gross_pay), percentage)
  end

  defp calculate_pension(gross_pay) do
    percentage = Decimal.from_float(0.08)
    Decimal.mult(Decimal.from_float(gross_pay), percentage)
  end

  defp calculate_gross_pay(attrs) do
    Enum.reduce(attrs, 0, fn(k, acc) -> elem(Float.parse(k["rate"]), 0) + acc end)
  end
  # ----------------------- End of Deductions ---------------------------- #



  def payroll do
    Repo.all(Ledgers)
  end
end
