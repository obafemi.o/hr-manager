defmodule BilloutWeb.Api.EmployeeRoleView do
    use BilloutWeb, :view

    def render("index.json", %{payroll: payroll}) do
      %{data: render_many(payroll, BilloutWeb.Api.EmployeeRoleView, "payroll.json")}
    end

    def render("payroll.json", %{payroll: payroll}) do
      %{
        employee_id: payroll.employee_id,
        employee_name: payroll.employee_name,
        payrun_id: payroll.payrun_id,
        earnings: payroll.earnings,
        deductions: payroll.deductions,
        net_pay: payroll.net_pay
      }
    end
end
