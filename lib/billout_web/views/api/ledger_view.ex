defmodule BilloutWeb.Api.LedgerView do
  use BilloutWeb, :view

  def render("index.json", %{payrolls: payrolls, total: total}) do
    %{
      total: total,
      payroll: render_many(payrolls, BilloutWeb.Api.PayrollView, "payroll.json")
    }
  end

  def render("payroll.json", %{payroll: payroll}) do
    %{
      employee_id: payroll.employee_id,
      employee_name: payroll.employee_name,
      deductions: payroll.deductions,
      earnings: payroll.earnings,
      net_pay: payroll.net_pay

    }
  end

  # def render("ledger.json", %{ledgers: ledgers}) do
  #   render_many(ledgers, BilloutWeb.Api.LedgerView, "ledger.json")
  # end

  def render("previous_payrolls.json", %{ledgers: ledgers}) do
    %{
      data: render_many(ledgers, BilloutWeb.Api.LedgerView, "ledger.json")
    }
  end

  def render("ledger.json", %{ledger: ledger}) do
    %{
      payrun_id: ledger.payrun_id
    }
  end
end
