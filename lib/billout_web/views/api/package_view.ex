defmodule BilloutWeb.Api.PackageView do
  use BilloutWeb, :view

  def render("list_packages.json", %{packages: packages}) do
    %{packages: render_many(packages, BilloutWeb.Api.PackageView, "package.json")}
  end

  def render("index.json", %{packages: packages, total: total}) do
    %{
      total: total,
      packages: render_many(packages, BilloutWeb.Api.PackageView, "package.json")
    }
  end

  def render("package.json", %{package: package}) do
    %{
      id: package.id,
      tag: package.tag,
      description: package.description
    }
  end

  def render("ledgers.json", %{ledgers: ledgers}) do
    %{ ledgers: render_many(ledgers, BilloutWeb.Api.PackageView, "ledger_list.json") }
  end

  def render("package_contents.json", %{ledgers: ledgers, id: id}) do
    %{
      id: id,
      earnings: render_many(ledgers.false, BilloutWeb.Api.PackageView, "earnings.json", as: :earnings),
      deductions: render_many(ledgers.true, BilloutWeb.Api.PackageView, "deductions.json", as: :deductions)
    }
  end

  def render("earnings.json", %{earnings: earnings}) do
    %{
      type: earnings.description,
      startDate: earnings.start_date,
      endDate: earnings.end_date,
      rate: earnings.value
    }
  end

  def render("deductions.json", %{deductions: deductions}) do
    %{
      type: deductions.description,
      startDate: deductions.start_date,
      endDate: deductions.end_date,
      rate: deductions.value
    }
  end
end
