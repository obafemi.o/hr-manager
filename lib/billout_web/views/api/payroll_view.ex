defmodule BilloutWeb.Api.PayrollView do
  use BilloutWeb, :view

    def render("index.json", %{payrolls: payrolls, total: total}) do
    %{
      total: total,
      payroll: render_many(payrolls, BilloutWeb.Api.PayrollView, "payroll.json")
    }
  end

  def render("user_index.json", %{payrolls: payrolls}) do
    %{
      payroll: render_many(payrolls, BilloutWeb.Api.PayrollView, "payroll.json")
    }
  end

  def render("previous_payrolls.json", %{payrolls: payrolls}) do
    %{
      payroll: render_many(payrolls, BilloutWeb.Api.PayrollView, "previous_payroll.json")
    }
  end

  def render("payruns.json", %{payrolls: payrolls}) do
    %{
      payroll: render_many(payrolls, BilloutWeb.Api.PayrollView, "payroll.json")
    }
  end

  def render("payroll.json", %{payroll: payroll}) do
    %{
      employee_id: payroll.employee_id,
      employee_name: payroll.employee_name,
      deductions: payroll.deductions,
      earnings: payroll.earnings,
      net_pay: payroll.net_pay,
      payrun_id: payroll.payrun_id
    }
  end

  # def render("payrun.json", %{payroll: payroll}) do
  #   %{
  #     employee_id: payroll.employee_id,
  #     employee_name: payroll.employee_name,
  #     deductions: payroll.deductions,
  #     earnings: payroll.earnings,
  #     net_pay: payroll.net_pay,
  #     payrun_id: payroll.payrun_id
  #   }
  # end

  def render("previous_payroll.json", %{payroll: payroll}) do
    %{
      payrun_id: payroll.payrun_id
    }
  end
end
