defmodule BilloutWeb.Api.PayrunView do
  use BilloutWeb, :view

  def render("show.json", %{payruns: payruns}) do
    %{payrun: render_many(payruns, BilloutWeb.Api.PayrunView, "payrun.json")}
  end

  def render("index.json", %{payruns: payruns}) do
    %{payruns: render_many(payruns, BilloutWeb.Api.PayrunView, "payruns.json")}
  end



  def render("payrun.json", %{payrun: payrun}) do
    %{
      employee_id: payrun.employee_id,
      employee_name: payrun.employee_name,
      deductions: payrun.deductions,
      earnings: payrun.earnings,
      net_pay: payrun.net_pay,
      payrun_id: payrun.payrun_id
    }
  end

  def render("payruns.json", %{payrun: payrun}) do
    %{
      employee_type: payrun.employee_type,
      end_date: payrun.end_date,
      id: payrun.id,
      ledgers: Enum.map(payrun.ledgers, fn ledger ->
        %{
          id: ledger.id,
          start_date: ledger.start_date,
          end_date: ledger.end_date,
          value: ledger.value,
          description: ledger.description,
          earnings: ledger.earnings,
          deductions: ledger.deductions,
          processed: ledger.processed,
          payrun_id: ledger.payrun_id,
          employee_id: ledger.employee_id
        }
      end),
      pay_type: payrun.pay_type,
      start_date: payrun.start_date
    }
  end
end
