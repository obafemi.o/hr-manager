defmodule BilloutWeb.Api.DeductionView do
  use BilloutWeb, :view


  def render("deductions.json", %{deductions: deductions}) do
    %{data: render_many(deductions, BilloutWeb.Api.DeductionView, "deduction.json")}
  end

  def render("employeepackages.json", %{employeepackage: employeepackage}) do
    %{data: render_one(employeepackage, BilloutWeb.Api.DeductionView, "employeepackage.json")}
  end

  def render("deduction.json", %{deduction: deduction}) do
    %{
      id: deduction.id,
      basis: deduction.basis,
      frequency: deduction.frequency,
      rate: deduction.rate,
      type: deduction.type,
      start_date: deduction.start_date,
      end_date: deduction.end_date,
      package_id: deduction.package_id
    }
  end

  def render("employeepackage.json", %{employeepackage: employeepackage}) do
    %{
      id: employeepackage.id,
      employee_id: employeepackage.employee_id,
      package: employeepackage.package.id
    }
  end
end
