defmodule BilloutWeb.Api.EmployeeView do
  use BilloutWeb, :view

  def render("index.json", %{employees: employees, total: total}) do
    %{
      total: total,
      employees: render_many(employees, BilloutWeb.Api.EmployeeView, "employee.json")
    }
  end

  def render("show.json", %{employee: employee}) do
    %{data: render_one(employee, BilloutWeb.Api.EmployeeView, "employee.json")}
  end

  def render("employee.json", %{employee: employee}) do
    %{
      id: employee.id,
      title: employee.title,
      name: employee.first_name <> " " <> employee.surname,
      first_name: employee.first_name,
      surname: employee.surname,
      middle_name: employee.middle_name,
      email: employee.email,
      phone: employee.phone,
      gender: employee.gender,
      address: employee.address,
      address2: employee.address_2,
      marital_status: employee.marital_status,
      date_of_birth: employee.dob,
      dob: employee.dob,
      country: employee.country,
      state: employee.state,
      children: employee.children,
      dependents: employee.dependents,
      role: employee.role,
      status: employee.status,
      account_number: employee.account_number,
      account_name: employee.account_name,
      bank_name: employee.bank_name,
      bank_country: employee.bank_country,
      branch_address: employee.branch_address,
      bank_country: employee.bank_country,
      sort_code: employee.sort_code,
      is_user: to_string(employee.is_user)
    }
  end

  def render("package.json", %{packages: packages}) do
    # IO.inspect "packages"
    # IO.inspect packages
  end
end
