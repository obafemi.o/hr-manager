defmodule BilloutWeb.Api.EarningView do
  use BilloutWeb, :view

  def render("package_earnings.json", %{earnings: earnings}) do
    render_many(earnings, BilloutWeb.Api.EarningView, "earning.json")
  end

  def render("packages.json", %{packages: packages}) do
    render_many(packages, BilloutWeb.Api.EarningView, "package.json", as: :package)
  end

  def render("earning.json", %{earning: earning}) do
    %{
      type: earning.type,
      startDate: earning.startDate,
      endDate: earning.endDate,
      frequency: earning.frequency,
      rate: earning.rate,
      basis: earning.basis
    }
  end

  def render("package.json", %{package: package}) do
    %{
      id: package.id,
      earnings: package.package.earnings
      # endDate: earning.endDate,
      # frequency: earning.frequency,
      # rate: earning.rate,
      # basis: earning.basis
    }
  end
end
