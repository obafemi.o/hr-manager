defmodule BilloutWeb.LayoutView do
  use BilloutWeb, :view

  alias Plug.Conn
  alias Pow.{Config, Operations}
  use Pow.Ecto.Schema
  alias Billout.Organization


  def current_username(conn) do
    id = Pow.Plug.current_user(conn).employee_id
    employee = Organization.get_employee!(id)
    employee.first_name <>" "<> employee.surname

  end

  def current_position(conn) do
    id = Pow.Plug.current_user(conn).employee_id
    employee = Organization.get_employee!(id)
    employee.position

  end
end
