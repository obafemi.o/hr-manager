defmodule BilloutWeb.PowResetPassword.MailerView do
  use BilloutWeb, :mailer_view

  def subject(:reset_password, _assigns), do: "Reset password link"
end
