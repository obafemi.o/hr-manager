defmodule BilloutWeb.Pow.Routes do
  use Pow.Phoenix.Routes
  alias BilloutWeb.Router.Helpers, as: Routes

  @impl true
  def user_not_authenticated_path(conn), do: Routes.login_path(conn, :index)
end
