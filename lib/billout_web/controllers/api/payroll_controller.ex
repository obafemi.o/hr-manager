defmodule BilloutWeb.Api.PayrollController do
use BilloutWeb, :controller

  alias Billout.Payroll

  def index(conn, params) do
    cond do
      params == %{} ->
        render(conn, "index.json", payrolls: [], total: [])
      params ->
        payrolls = Payroll.get_max_payroll()
        total = Payroll.get_ledger_total()
        render(conn, "index.json", payrolls: payrolls, total: total)

      true ->
        render(conn, "index.json", payrolls: [], total: [])
    end
  end

  def user_index(conn, _params) do
    user = Pow.Plug.current_user(conn).employee_id
    payrolls = Billout.Payroll.list_employee_payroll(user)
    render(conn, "user_index.json", payrolls: payrolls)
  end

  def previous(conn, %{"start_date" => start_date, "end_date"=> end_date}) do
    payrolls = Billout.Payroll.previous_payroll(start_date, end_date)
    render(conn, "previous_payrolls.json", payrolls: payrolls)
  end

  def previous_payrun(conn, %{"payrun_id" => payrun_id}) do
    payrolls = Billout.Payroll.list_previous_payroll(payrun_id)
    IO.inspect payrolls
    render(conn, "payruns.json", payrolls: payrolls)
  end
end
