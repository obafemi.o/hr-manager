defmodule BilloutWeb.Api.EmployeeRoleController do
  use BilloutWeb, :controller

  alias Billout.Payroll

  def index(conn, _params) do
    user = Pow.Plug.current_user(conn).employee_id
    payroll = Billout.Payroll.list_employee_payroll(user)
    render(conn, "index.json", payroll: payroll)
  end

end
