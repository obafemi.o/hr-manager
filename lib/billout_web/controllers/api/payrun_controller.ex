defmodule BilloutWeb.Api.PayrunController do
  use BilloutWeb, :controller

  alias Billout.Payroll

  def index(conn, _) do
    payruns = Payroll.list_payruns()
    render(conn, "index.json", payruns: payruns)
  end
  def create(conn, %{"payrun" => payrun_params}) do
    IO.inspect payrun_params
    case Payroll.create_payrun(payrun_params) do
      {:ok, payrun} ->
        conn
        |> json(payrun)

      {:error, _} ->
        json conn, "Error Detected"
    end
  end

  def show(conn, %{"id" => id}) do
    payruns = Payroll.list_payroll(id)
    render(conn, "show.json", payruns: payruns)
  end

end
