defmodule BilloutWeb.Api.SettingsController do
  use BilloutWeb, :controller
  alias Billout.Authorization


  def add_role(conn, %{"role" => role_params, "permissions" => permission_params}) do   
    case Authorization.create_role(role_params) do
    {:error, %Ecto.Changeset{} = changeset} ->
       json conn, "There was an error"        
    {:ok, role} ->
      role_id = role.id
      case Authorization.create_multiple_permissions(role_id, permission_params) do
        {:ok, permission} ->
         conn
         _->
        conn        
          json conn, "There was an error"            
        end   
    end
  end

  def edit_role(conn, %{"role" => role_id, "permissions" => permission_params}) do 
    IO.inspect role_id
    # role = Authentication.get_role!(id) 
    case Authorization.upsert_permissions(role_id, permission_params) do
      {:ok, permission} ->
        json conn, %{permission: permission}
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
      _->
        conn
        json conn,"ok"
    end
    # case Authorization.create_role(role_params) do
    # {:error, %Ecto.Changeset{} = changeset} ->
    #    json conn, "There was an error"        
    # {:ok, role} ->
    #   role_id = role.id
    #   case Authorization.create_multiple_permissions(role_id, permission_params) do
    #     {:ok, permission} ->
    #      conn
    #      _->
    #     conn        
    #       json conn, "There was an error"            
    #     end   
    # end
  end

  def get_roles(conn, _params) do
    roles = Authorization.list_role()
      json(conn, %{roles: roles})
  end

end
