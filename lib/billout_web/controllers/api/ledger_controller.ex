defmodule BilloutWeb.Api.LedgerController do
  use BilloutWeb, :controller

  alias Billout.Payroll

  def create(conn, %{"ledger_params" => ledger_params}) do
    IO.inspect ledger_params
    case Payroll.create_ledger(ledger_params) do
      true ->
        payrolls = Payroll.get_max_payroll()
        total = Payroll.get_ledger_total()
        render(conn, "index.json", payrolls: payrolls, total: total)
        # conn
        # |> put_status(:created)
        # |> json(%{message: "Success"})
        # render(conn, %{message: "Success"})
      false ->
        conn
        |> put_status(:internal_server_error)
        |> json(%{message: "Error"})
        # render(conn, %{message: "Error"})
    end
  end

  def previous(conn, %{"start_date" => start_date, "end_date"=> end_date}) do
    payrolls = Billout.Payroll.previous_payroll(start_date, end_date)
    render(conn, "previous_ledger.json", payrolls: payrolls)
  end

end
