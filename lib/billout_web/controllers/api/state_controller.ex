defmodule BilloutWeb.Api.StateController do
  use BilloutWeb, :controller

  alias Billout.Location

  def index(conn, _params) do
    states = Location.list_states()
    render(conn, "index.json", states: states)
  end

  def state_country(conn, %{"country_id" => country_id}) do
    states = Location.get_state_country!(country_id)
    render(conn, "state_country.json", states: states)
  end
end
