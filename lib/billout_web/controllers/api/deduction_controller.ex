defmodule BilloutWeb.Api.DeductionController do
  use BilloutWeb, :controller

  alias Billout.Organization

  def create(conn, %{"deduction" => deduction_params}) do
    case Organization.create_deduction_record(deduction_params) do
      {:ok, _deduction} ->
        json(conn, %{})

      {:error, %Ecto.Changeset{} = changeset} ->
        json(conn, %{changeset: changeset.errors[:title]})
    end
  end

  def package_deductions(conn, %{"package_id" => package_id}) do
    deductions = Organization.get_deductions!(package_id)
    render(conn, "deductions.json", deductions: deductions)
  end

  def employee_deductions(conn, %{"employee_id" => employee_id}) do
    employeepackage = Organization.get_employee_package(employee_id)
    render(conn, "employeepackage.json", employeepackage: employeepackage)
  end

end
