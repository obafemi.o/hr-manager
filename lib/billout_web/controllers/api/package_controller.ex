defmodule BilloutWeb.Api.PackageController do
  use BilloutWeb, :controller

  alias Billout.{Organization, Payroll}

  def list_packages(conn, _params) do
    packages = Organization.list_packages()
    render(conn, "list_packages.json", packages: packages)
  end

  def create(conn, %{"id"=>id, "package" => package_params}) do
    case Organization.link_employee_package(id, package_params) do
      {:ok, package} ->
        json(conn, %{package: package})

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def index(conn, params) do
    cond do
      params == %{} ->
        render(conn, "index.json", packages: [], total: [])

      params["page"] ->
        page = String.to_integer(params["page"])
        offset = if page == 1, do: 0, else: (page - 1) * 25
        packages = Organization.list_package_templates(offset)
        total = Organization.get_package_total()
        render(conn, "index.json", packages: packages, total: total)

      true ->
        render(conn, "index.json", packages: [], total: [])
    end
  end

  def show(conn, %{"id" => id, "start_date" => start_date, "end_date" => end_date}) do
    case Payroll.get_employee_payroll(id, start_date, end_date) do
      nil ->
        json(conn, [])
      [] ->
        json(conn,%{id: id, earnings: [], deductions: []})
      ledgers ->
        render(conn, "package_contents.json", ledgers: parseLedger(ledgers), id: id)
    end
  end

  defp parseLedger(ledgers) do
    ledgers
    |> Enum.group_by(fn param -> Decimal.negative?(param.value) end)
  end

  def create_employee_package(conn, %{"employee_id" => employee_id, "package_id" => package_id}) do
    case Organization.create_employee_package(employee_id, package_id) do
      {:ok, employee_package} ->
      json conn, %{employee_package: employee_package}
      {:error, %Ecto.Changeset{} = changeset} ->
        # render(conn, "new.html", changeset: changeset)
      json conn, %{changeset: changeset.errors["Package not Assigned"]}
    end
  end

  def delete(conn, %{"id" => id}) do
    package = Organization.get_package!(id)
    {:ok, _package} = Organization.delete_package(package)

    conn
    |> redirect(to: Routes.settings_path(conn, :package))
  end


  def update(conn, %{"id" => id, "package" => package_params}) do
    IO.inspect package_params
    package = Organization.get_package!(id)
    IO.inspect package

    case Organization.update_package(package, package_params) do
      {:ok, _package} ->
        json(conn, "Package Update")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", package: package, changeset: changeset)
    end
  end

  def create_package(conn, %{"package" => package_params}) do
    case Organization.create_package(package_params) do
      {:ok, package} ->
        conn
        |> json(package)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

end

# def update(conn, %{"id" => id, "package" => package_params}) do
#     package = Organization.get_package!(id)

#     case Organization.update_package(package, package_params) do
#       {:ok, package} ->
#         conn
#         |> put_flash(:info, "Package updated successfully.")
#         |> redirect(to: Routes.package_path(conn, :show, package))

#       {:error, %Ecto.Changeset{} = changeset} ->
#         render(conn, "edit.html", package: package, changeset: changeset)
#     end
#   end
