defmodule BilloutWeb.LogoutController do
  use BilloutWeb, :controller

  def index(conn, _params) do
    conn
    |> redirect(to: Routes.login_path(conn, :index))
    |> Pow.Plug.clear_authenticated_user()
  end
end
