defmodule BilloutWeb.ForgotPasswordController do
  use BilloutWeb, :controller

  alias Billout.Repo
  alias PowResetPassword.{Phoenix.Mailer, Plug}
  alias Pow.{Config, Plug, Store.Backend.EtsCache, UUID}
  alias PowResetPassword.{Ecto.Schema, Store.ResetTokenCache}
  alias PowResetPassword.Ecto.Context
  alias PowResetPassword.Ecto.Schema
  alias Pow.Ecto.Context
  alias Pow.Phoenix.Routes
  use Pow.Extension.Phoenix.Controller.Base

  alias Plug.Conn

  def load_email(conn,  _params) do
    changeset= PowResetPassword.Plug.change_user(conn)
    conn
     |> render("new.html", changeset: changeset, layout: {BilloutWeb.LayoutView, "login.html"}, msg: "")
  end

  def process_create(conn, %{"user" => user_params}) do
    PowResetPassword.Plug.create_reset_token(conn, user_params)
  end

  @spec respond_create({:ok | :error, map(), Conn.t()}) :: Conn.t()
  def respond_create({:ok, %{token: token, user: user}, conn}) do
    url = routes(conn).url_for(conn, __MODULE__, :edit, [token])
    deliver_email(conn, user, url)

    default_respond_create(conn)
  end

  defp deliver_email(conn, user, url) do
    email = Mailer.reset_password(conn, user, url)

    Pow.Phoenix.Mailer.deliver(conn, email)
  end
  defp default_respond_create(conn) do
    conn
    |> assign(:msg, "Success")
    |> put_flash(:info, extension_messages(conn).email_has_been_sent(conn))
    |> redirect(to: Routes.session_path(conn, :new))
  end
end
