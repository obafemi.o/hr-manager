defmodule BilloutWeb.SettingsController do
  use BilloutWeb, :controller
  alias Billout.Organization
  alias Billout.Organization.Package
  alias Billout.Authorization

  def index(conn, _params) do
    render(conn, :index)
  end

  def package(conn, _params) do
    render(conn, :package)
  end

  def manage_role(conn, _params) do
    render(conn, :roles)
  end

   
end
