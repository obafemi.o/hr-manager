defmodule BilloutWeb.EmployeeController do
  use BilloutWeb, :controller

  alias Billout.Organization
  alias Billout.Organization.Employee
  alias NimbleCSV.RFC4180, as: CSV
  alias Billout.Repo
  alias PowResetPassword.Plug
  alias Pow.{Config, Plug, Store.Backend.EtsCache, UUID}
  alias PowResetPassword.{Ecto.Schema, Store.ResetTokenCache}
  alias PowResetPassword.Ecto.Context
  alias PowResetPassword.Ecto.Schema
  alias Pow.Ecto.Context
  alias Billout.Users.User

  # Check if user is allowed access
  plug Bodyguard.Plug.Authorize,
  policy: Billout.Policy,
  action: {Phoenix.Controller, :action_name},
  user: {Pow.Plug, :current_user},
  fallback: BilloutWeb.FallbackController


  @spec index(Plug.Conn.t(), keyword | map) :: Plug.Conn.t()
  def index(conn, params) do
    is_first_time = Pow.Plug.current_user(conn).is_first_time
    current_user = Pow.Plug.current_user(conn)
    count = Repo.aggregate(Employee, :count, :id)
    search = Organization.search(params)
    page = Employee |> Repo.paginate(params)
    render(conn, "index.html", employees: page.entries, is_first_time: is_first_time, page: page, count: count, search: search, current_user: current_user)
  end

  def sort(conn, _params) do
    employees = Organization.list_employees_desc()
    render(conn, "index.html", employees: employees)
  end

  @spec new(Plug.Conn.t(), any) :: Plug.Conn.t()
  def new(conn, _params) do
    changeset = Organization.change_employee(%Employee{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"employee" => employee_params}) do
    create_as_user = Map.get(employee_params, "create_as_user" )

    case Organization.create_employee(employee_params) do
      {:ok, employee} ->
        # Create user if checkbox is ticked
        if (create_as_user ) do
          creation_email = Billout.Email.create(employee.email, "User Account Created", "<div style='margin: auto; width: 70%'><div style='background-color: #152935; margin: auto;color: #fff;text-align: center; padding-top: 5px; padding-bottom: 5px'><h4>Billout</h4></div><div style='margin-left: 20px'><p>Hello "<>employee.first_name<>" "<>employee.surname<>",</p><p>An account has been created for you on the Billout</p><p>Below are your login details</p><p>Email: "<>employee.email<>"</p><p>Password: 1234567890</p></div><div style='text-align: center; margin-bottom: 150px'><p>Click the button below to Login</p><br>
            <a href='192.168.1.42:4000/login' style='background-color: #152935;color: #fff; padding: 10px 42px; text-decoration: none'>Login</a></div><div style='text-align: center; margin-bottom: 20px'>Powered by Lopworks limited</div></div>")
          # Billout.Mailer.deliver_now(creation_email)
          employee_id = employee.id
          email = employee.email
          role = employee.role
          attrs= %{is_user: true}
          conn
          |> Pow.Plug.create_user(%{employee_id: employee_id, email: email, password: "1234567890", confirm_password: "1234567890", role: role, is_first_time: true })
          Billout.Mailer.deliver_now(creation_email)
          Organization.update_employee(employee, attrs)
        end
        json(conn, %{employee: employee})

      {:error, %Ecto.Changeset{} = changeset} ->
        json(conn, %{changeset: changeset.errors[:title]})
    end
  end

  @spec new_upload(Plug.Conn.t(), any) :: Plug.Conn.t()
  def new_upload(conn, _params) do
    render(conn, "upload.html", conn: conn)
  end

  def download_sample(conn, _params) do
    file = File.read!("priv/samples/sample.csv")

    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", "attachment; filename=sample.csv")
    |> send_resp(200, file)
  end

  def upload(conn, %{"employees" => employee_params}) do
    employee_params.path
    |> File.read!()
    |> CSV.parse_string()
    |> validate_file_data(conn)
  end

  defp validate_file_data(params, conn) do
    c =
      Enum.all?(params, fn [title, first_name,surname,middle_name,email,country,state,gender,role,phone,
                             address,address_2,dob,marital_status,children,dependents, status, account_name,
                             account_number, bank_name, branch_address, sort_code, bank_country] ->
        employee = %{
          "account_name" => account_name,
          "account_number" => account_number,
          "address" => address,
          "address_2" => address_2,
          "bank_country" => bank_country,
          "bank_name" => bank_name,
          "branch_address" => branch_address,
          "children" => children,
          "country" => country,
          "dependents" => String.replace(dependents, "\r", ""),
          "dob" => dob,
          "email" => email,
          "first_name" => first_name,
          "gender" => gender,
          "marital_status" => marital_status,
          "middle_name" => middle_name,
          "phone" => phone,
          "role" => role,
          "sort_code" => sort_code,
          "state" => state,
          "status" => status,
          "surname" => surname,
          "title" => title
        }

        Organization.is_file_data_valid?(employee)
      end)

    case c do
      true -> create_file_data(params, conn)
      false -> send_resp(conn, 500, "Error")
    end
  end

  defp create_file_data(params, conn) do
    Enum.each(params, fn[title, first_name,surname,middle_name,email,country,state,gender,role,phone,
                             address,address_2,dob,marital_status,children,dependents, status, account_name,
                             account_number, bank_name, branch_address, sort_code, bank_country] ->
      employee = %{
          "account_name" => account_name,
          "account_number" => account_number,
          "address" => address,
          "address_2" => address_2,
          "bank_country" => bank_country,
          "bank_name" => bank_name,
          "branch_address" => branch_address,
          "children" => children,
          "country" => country,
          "dependents" => String.replace(dependents, "\r", ""),
          "dob" => dob,
          "email" => email,
          "first_name" => first_name,
          "gender" => gender,
          "marital_status" => marital_status,
          "middle_name" => middle_name,
          "phone" => phone,
          "role" => role,
          "sort_code" => sort_code,
          "state" => state,
          "status" => status,
          "surname" => surname,
          "title" => title
        }

      case Organization.create_employee(employee) do
        {:ok, _} ->
          :ok

        {:error, _} ->
          send_resp(conn, 500, "Error")
          :break
      end
    end)

    send_resp(conn, 200, "Success")
  end

  def show(conn, %{"id" => id}) do
    employee = Organization.get_employee!(id)
    # render(conn, "show.html", employee: employee)
    json(conn, %{employee: employee})
  end

  def retrieve(conn, %{"id" => id}) do
    employee_package = Organization.get_employee_package(id)
    # render(conn, "show.html", employee: employee)
    IO.inspect(employee_package)
    json(conn, %{employee_package: employee_package})
  end

  def edit(conn, %{"id" => id}) do
    employee = Organization.get_employee!(id)
    changeset = Organization.change_employee(employee)
    render(conn, "edit.html", employee: employee, changeset: changeset)
  end

  def update(conn, %{"employee" => employee_params}) do
    id = Map.get(employee_params, "id")
    employee = Organization.get_employee!(id)

    case Organization.update_employee(employee, employee_params) do
      {:ok, employee} ->
        json(conn, %{employee: employee})

      {:error, %Ecto.Changeset{} = changeset} ->
        json(conn, %{changeset: changeset.errors[:title]})
        # render(conn, "edit.html", employee: employee, changeset: changeset)
    end
  end

  # def update(conn, %{"id" => id, "employee" => employee_params}) do
  #   employee = Organization.get_employee!(id)

  #   case Organization.update_employee(employee, employee_params) do
  #     {:ok, employee} ->
  #       conn
  #       |> put_flash(:info, "Employee updated successfully.")
  #       |> redirect(to: Routes.employee_path(conn, :show, employee))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "edit.html", employee: employee, changeset: changeset)
  #   end
  # end

  def delete(conn, %{"id" => id}) do
    employee = Organization.get_employee!(id)
    {:ok, _employee} = Organization.delete_employee(employee)

    conn
    |> put_flash(:info, "Employee deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :index))
  end

  def assign(conn, %{package_id: pid, employee_id: eid, start_date: sd, end_date: ed}) do
    case Organization.assign_package(pid, eid, sd, ed) do
      {:ok, employee_package} ->
        json(conn, %{employee_package: employee_package})

      {:error, _} ->
        json(conn, %{})
    end
  end

  def change_role(conn, %{"user" => user_params}) do    
    config = Plug.fetch_config(conn)
    case Organization.change_employee_role(user_params, config) do
      {:ok,  user} ->
        json(conn, %{})
    end
  end

  def change_roles(conn, %{"role" => role_params, "employees" => employees_params}) do  
    config = Plug.fetch_config(conn)    
    case Organization.change_multiple_employee_roles( employees_params, role_params, config) do
      _->
        json(conn, %{})
    end
  end

#................Pow pasword reset override functions below .................#

  @spec load_reset(Plug.Conn.t(), any) :: Plug.Conn.t()
  def load_reset(conn,  _params) do
    changeset= PowResetPassword.Plug.change_user(conn)
    conn
     |> render("reset.html", changeset: changeset, layout: {BilloutWeb.LayoutView, "login.html"}, msg: "")
  end

  @spec handle_reset(Plug.Conn.t(), map) :: Plug.Conn.t()
  def handle_reset(conn, %{"user" => user_params}) do
    password = Map.get(user_params, "password" );
    confirm_password = Map.get(user_params, "confirm_password" );
    if password == confirm_password  do
      config = Pow.Plug.fetch_config(conn)
      current_user = Pow.Plug.current_user(conn)
      case maybe_create_reset_token(conn, current_user, config) do
          {:ok, token, user} ->
          user_params= Map.put(user_params, "is_first_time", false)
          update_user_password(conn, user_params)
          conn
          |> put_flash(:info, "Password Successfully Reset'")
          |> redirect(to: Routes.login_path(conn, :index))
          |> Pow.Plug.clear_authenticated_user()
      end
    end
    if password == "" || confirm_password == "" ||  password !== confirm_password  do
      changeset = PowResetPassword.Plug.change_user(conn)
      conn
      |> render("reset.html", changeset: changeset, layout: {BilloutWeb.LayoutView, "login.html"}, msg: "reset_error")
    end
  end
  defp maybe_create_reset_token(conn, nil, _config) do
    changeset = PowResetPassword.Plug.change_user(conn)
    {:error, %{changeset | action: :update}, conn}
  end

  defp maybe_create_reset_token(conn, user, config) do
    token = UUID.generate()
    {store, store_config} = store(config)
    store.put(store_config, token, user)
    {:ok, %{token: token, user: user}, conn}
  end

  defp store(config) do
    backend = Config.get(config, :cache_store_backend, EtsCache)
    {ResetTokenCache, [backend: backend]}
  end

  def update_user_password(conn, params) do
    config = Plug.fetch_config(conn)
    token  = conn.params["id"]
    conn
      |> PowResetPassword.Plug.assign_reset_password_user(conn)
      |> update_password_schema(params, config)
      |> maybe_expire_token(conn, token, config)
  end

  defp maybe_expire_token({:ok, user}, conn, token, config) do
    expire_token(token, config)
    {:ok, user, conn}
  end

  defp maybe_expire_token({:error, changeset}, conn, _token, _config) do
    {:error, changeset, conn}
  end

  defp expire_token(token, config) do
    {store, store_config} = store(config)
    store.delete(store_config, token)
  end

  defp update_password_schema(conn, params, config) do
    case update_user_with_changeset(conn, params, config) do
    {:ok, user} ->
      attrs= %{is_first_time: false}
      %User{}
      |> User.changeset(attrs)
      |> Pow.Ecto.Context.do_update(config)
    end
  end
  defp  update_user_with_changeset(conn, params, config) do
    user = Pow.Plug.current_user(conn)
    user
    |> User.reset_password_changeset(params)
    |> Pow.Ecto.Context.do_update(config)
  end
  #................Pow pasword reset override functions above.................#
end


