defmodule BilloutWeb.FallbackController do
  use BilloutWeb, :controller

  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_status(302)
    |> redirect(to: Routes.login_path(conn, :index))

  end
end
