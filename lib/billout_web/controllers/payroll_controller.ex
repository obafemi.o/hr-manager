defmodule BilloutWeb.PayrollController do
  use BilloutWeb, :controller

  def index(conn, _params) do
    render(conn, :index)
  end
end
