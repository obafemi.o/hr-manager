defmodule BilloutWeb.EmployeeRoleController do
  use BilloutWeb, :controller

  alias Billout.Payroll
  alias Billout.Organization
  plug :put_layout, "employee_app.html"



  def index(conn, _params) do
    user = Pow.Plug.current_user(conn).employee_id
    # user_id = Organization.get_employee!(user)
    # payroll = Payroll.list_employee_payroll(user)
    render(conn, "index.html", user: user)
  end

end
