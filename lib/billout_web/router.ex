defmodule BilloutWeb.Router do
  use BilloutWeb, :router
  use Pow.Phoenix.Router
  use Pow.Extension.Phoenix.Router, otp_app: :billout


  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers

  end

    pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
      error_handler: Pow.Phoenix.PlugErrorHandler
  end

  pipeline :api do
    plug :accepts, ["json"]
  end


  scope "/" do
    pipe_through :browser
    pow_extension_routes()
    resources "/login", BilloutWeb.LoginController
    pow_routes()

  end

 # Scopes that require authentication
  scope "/", BilloutWeb do
    pipe_through [:browser, :protected]
    get "/logout", LogoutController, :index
    post "/employees/role", EmployeeController, :change_role #Route for obsolete role tab
    post "/employees/roles", EmployeeController, :change_roles
    get "/employees/reset", EmployeeController, :load_reset
    post "/employees/reset", EmployeeController, :handle_reset
    post "/employees", EmployeeController, :create
    get "/employees/package/:id", EmployeeController, :retrieve
    get "/", PageController, :index
    get "/employees/sample/download", EmployeeController, :download_sample
    get "/employees/upload", EmployeeController, :new_upload
    post "/employees/upload", EmployeeController, :upload
    put "/employees", EmployeeController, :update
    resources "/employees", EmployeeController
    get "/employees/sort", EmployeeController, :sort
    get "/settings", SettingsController, :index
    get "/settings/roles", SettingsController, :manage_role
    get "/settings/packages", SettingsController, :package
    resources "/payroll", PayrollController
    resources "/user/payroll", EmployeeRoleController

  end

  # Other scopes may use custom stacks.
  scope "/api", BilloutWeb.Api, as: :api do
    pipe_through [:api, :protected]
    resources "/payroll", PayrollController
    resources "/earnings", EarningController
    resources "/employees", EmployeeController
    resources "/packages", PackageController
    post "/package", PackageController, :create_package
    resources "/countries", CountryController, only: [:index, :show]
    get "/states/:country_id", StateController, :state_country
    get "/package_earnings/:package_id", EarningController, :package_earnings
    get "/list_packages", PackageController, :list_packages
    post "/assign_package", PackageController, :create_employee_package
    post "/earnings/upsert", EarningController, :update_or_insert
    post "/ledger", LedgerController, :create
    get "/user/payroll", PayrollController, :user_index
    post "/deduction", DeductionController, :create
    post "/settings/new_role", SettingsController, :add_role
    post "/settings/change_role", SettingsController, :edit_role
    get "/deductions/:package_id", DeductionController, :package_deductions
    get "/employeedeductions/:employee_id", DeductionController, :employee_deductions
    get "/settings/get_role", SettingsController, :get_roles
    get "/ledger/previous", PayrollController, :previous
    get "/payroll/previous/:payrun_id", PayrollController, :previous_payrun
    get "/earnings/employee/:employee_id", EarningController, :employee_earnings
    resources "/payrun", PayrunController
  end


end
