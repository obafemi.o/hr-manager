defmodule Billout.Repo.Migrations.UpdateLegderTable do
  use Ecto.Migration

  def change do
    alter table(:ledgers) do      
      add :earnings, :decimal
      add :deductions, :decimal
      add :processed, :boolean, default: false    
    end
  end
end
