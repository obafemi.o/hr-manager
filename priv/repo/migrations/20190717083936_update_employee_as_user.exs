defmodule Billout.Repo.Migrations.UpdateEmployeeAsUser do
  use Ecto.Migration

  def change do
    alter table(:employees) do
      add :position, :string
      add :password_hash, :string
      modify :email, :string, null: false
    end
  end
end
