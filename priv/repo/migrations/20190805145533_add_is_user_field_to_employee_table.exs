defmodule Billout.Repo.Migrations.AddIsUserFieldToEmployeeTable do
  use Ecto.Migration

  def change do
    alter table(:employees) do
      add :is_user, :boolean, default: false
    end
  end
end
