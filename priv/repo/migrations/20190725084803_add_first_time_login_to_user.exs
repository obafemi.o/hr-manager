defmodule Billout.Repo.Migrations.AddFirstTimeLoginToUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :is_first_time, :boolean, default: true
    end
  end
end
