defmodule Billout.Repo.Migrations.AddAccountDetailsToEmployee do
  use Ecto.Migration

  def change do
    alter table(:employees) do
      add :account_name, :string
      add :account_number, :string
      add :bank_name, :string
      add :branch_address, :string
      add :sort_code, :string
      add :bank_country, :string
    end
  end
end
