defmodule Billout.Repo.Migrations.CreatePackages do
  use Ecto.Migration

  def change do
    create table(:packages) do
      add :tag, :string
      # add :earnings, {:array, :jsonb}, default: []

      timestamps()
    end

    # alter table(:packages) do
    #   add :tag, :string
    # end
  end
end
