defmodule Billout.Repo.Migrations.UpdateLedgerTable2 do
  use Ecto.Migration

  def change do
    alter table(:ledgers) do  
      remove :payrun_id      
      add :payrun_id, references(:payruns, on_delete: :delete_all)
      add :date, :date
    end
  end
end
