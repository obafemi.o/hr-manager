defmodule Billout.Repo.Migrations.CreateEmployeePackage do
  use Ecto.Migration

  def change do
    create table(:employee_package) do
      add :employee_id, references(:employees, on_delete: :delete_all)
      add :package_id, references(:packages, on_delete: :delete_all)

      timestamps()
    end

    create index(:employee_package, [:employee_id])
    create index(:employee_package, [:package_id])
    create unique_index(:employee_package, [:employee_id, :package_id])
  end
end
