defmodule Billout.Repo.Migrations.CreateDeductions do
  use Ecto.Migration

  def change do
    create table(:deductions) do
      add :basis, :string
      add :frequency, :string
      add :rate, :string
      add :type, :string
      add :start_date, :date
      add :end_date, :date
      add :package_id, references(:packages, on_delete: :nothing)

      timestamps()
    end

    create index(:deductions, [:package_id])
  end
end
