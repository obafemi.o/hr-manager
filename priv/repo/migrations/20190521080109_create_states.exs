defmodule Billout.Repo.Migrations.CreateStates do
  use Ecto.Migration

  def change do
    create table(:states) do
      add :name, :string
      add :country_id, :string

      timestamps()
    end
  end
end
