defmodule Billout.Repo.Migrations.UpdateDeductionRateColumn do
  use Ecto.Migration

  def change do
    execute """
        alter table deductions alter column rate type decimal using (rate::decimal)
       """
  end
end
