defmodule Billout.Repo.Migrations.UpdatePayrunIdOnLedgerTable do
  use Ecto.Migration

  def change do
    execute """
        alter table ledgers alter column payrun_id type decimal using (payrun_id::numeric)
       """
  end
end
