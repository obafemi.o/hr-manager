defmodule Billout.Repo.Migrations.UpdateLedgerTable do
  use Ecto.Migration

  def change do
    alter table(:ledgers) do
      add :employee_id, references(:employees, on_delete: :nothing)
    end
  end
end
