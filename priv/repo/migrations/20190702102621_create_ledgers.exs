defmodule Billout.Repo.Migrations.CreateLedgers do
  use Ecto.Migration

  def change do
    create table(:ledgers) do
      add :value, :decimal
      add :description, :text
      add :payrun_id, :string
      add :start_date, :date
      add :end_date, :date

      timestamps()
    end

    create index(:ledgers, [:payrun_id])
  end
end
