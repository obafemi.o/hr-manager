defmodule Billout.Repo.Migrations.UpdateEmployeesTable do
  use Ecto.Migration

  def change do
    rename table(:employees), :dependants, to: :dependents
  end
end
