defmodule Billout.Repo.Migrations.CreatePayruns do
  use Ecto.Migration

  def change do
    create table(:payruns) do
      add :start_date, :date
      add :end_date, :date
      add :pay_type, :string
      add :employee_type, :string

      timestamps()
    end

  end
end
