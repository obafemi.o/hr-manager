defmodule Billout.Repo.Migrations.UpdatePowUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :employee_id, references(:employees, on_delete: :nothing)
      add :role, :string
    end
  end
end
