defmodule Billout.Repo.Migrations.ModifyPackageTable do
  use Ecto.Migration

  def change do
    alter table(:packages) do
      add :description, :string
    end
  end
end
