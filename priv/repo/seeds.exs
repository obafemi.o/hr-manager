alias Billout.Organization
alias Billout.Organization.{Package, Earning}
alias NimbleCSV.RFC4180, as: CSV
alias Billout.Location.Country
alias Billout.Location.State
alias Billout.Repo
alias Billout.Users.User
alias Billout.Authorization.Role
alias Billout.Authorization.Permission


# employee_data = %{
#     first_name: Faker.Name.first_name(),
#     middle_name: Faker.Name.first_name(),
#     surname: Faker.Name.last_name(),
#     email: Faker.Internet.email(),
#     position: Enum.random(["Engineer", "Lawyer", "Doctor"]),
#     gender: Enum.random(["Male", "Female"]),
#     phone: Faker.Phone.EnGb.number(),
#     address: Faker.Address.street_address(),
#     role: Enum.random(["Employee", "HR"]),
#     state: Faker.Address.state(),
#     country: Faker.Address.country(),
#     dob: Faker.Date.date_of_birth(18..65),
#     marital_status: Enum.random(["Single", "Married", "Confused"]),
#     positon: Enum.random(["App Dev", "Sales", "Account"]),
#     password: "secret",
#     title: Enum.random(["Mr", "Mrs", "Dr", "Ms", "Miss"]),
#     children: Enum.random([0, 1, 2, 3, 4]),
#     dependents: Enum.random([0, 1, 2, 3]),
#     status:  Enum.random(["Employee", "Contractor", "Director", "Retired", "Resigned", "Discharged"]),
#   }

#   %Organization.Employee{}
#   |> Organization.Employee.changeset(employee_data)
#   |> Repo.insert!()

for data <- 1..100 do
  employee_data = %{
    first_name: Faker.Name.first_name(),
    middle_name: Faker.Name.first_name(),
    surname: Faker.Name.last_name(),
    email: Faker.Internet.email(),
    position: Enum.random(["Engineer", "Lawyer", "Doctor"]),
    gender: Enum.random(["Male", "Female"]),
    phone: Faker.Phone.EnGb.number(),
    address: Faker.Address.street_address(),
    role: Enum.random(["Employee", "HR"]),
    state: Faker.Address.state(),
    country: Faker.Address.country(),
    dob: Faker.Date.date_of_birth(18..65),
    marital_status: Enum.random(["Single", "Married", "Confused"]),
    positon: Enum.random(["App Dev", "Sales", "Account"]),
    password: "secret",
    title: Enum.random(["Mr", "Mrs", "Dr", "Ms", "Miss"]),
    children: Enum.random([0, 1, 2, 3, 4]),
    dependents: Enum.random([0, 1, 2, 3]),
    status:  Enum.random(["Employee", "Contractor", "Director", "Retired", "Resigned", "Discharged"]),
  }

  %Organization.Employee{}
  |> Organization.Employee.changeset(employee_data)
  |> Repo.insert!()
end

for data <- 1..10 do
  package =  %{
    tag:  Enum.random(["Intern", "Developer", "Sales"]),
    description: Faker.Lorem.sentence()
  }

  %Package{}
  |> Package.changeset(package)
  |> Repo.insert!
end

for data <- 1..50 do
  earning = %{
    basis: "Monthly",
    frequency: "Monthly",
    rate: Enum.random(["200000", "150000", "100000"]),
    type: Enum.random(["Basic", "Housing", "Transportaion", "Commission", "Bonus"]),
    startDate: Faker.Date.between(~D[2019-05-30], ~D[2019-07-30]),
    endDate: Faker.Date.between(~D[2019-06-01], ~D[2019-08-30]),
    package_id: Enum.random([1,2,3,4,5,6,7,8,9,10])
  }

  %Earning{}
  |> Earning.changeset(earning)
  |> Repo.insert!
end

"priv/seed_data/countries.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [name,iso3,iso2,phonecode,capital,currency]->
  %Country{name: name,iso3: iso3, iso2: iso2, phonecode: phonecode, capital: capital,currency: currency}
  |> Repo.insert
end)

"priv/seed_data/states.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [name,country_id]->
  %State{name: name,country_id: country_id}
  |> Repo.insert
end)

# Seed Pow Default User
  user= %{
        email: "admin@gmail.com",
        password: "1234567890",
        confirm_password: "1234567890",
        password_hash: Pow.Ecto.Schema.Password.pbkdf2_hash("1234567890"),
        role: "HR",
        employee_id: 9,
        is_first_time: false
    }

  %User{}
    |> User.changeset(user)
    |> Repo.insert()


# Seed Roles and Permissions

"priv/seed_data/role.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [name]->
  %Role{name: name}
  |> Repo.insert
end)

"priv/seed_data/permissions.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [name,action,role_id]->
  %Permission{name: name, action: action, role_id: String.to_integer(role_id)}
  |> Repo.insert
end)


# [{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"4500","startDate":"2019-10-01","type":"Benefit"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"9000","startDate":"2019-10-01","type":"Car"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"15000","startDate":"2019-10-01","type":"Telephone"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"6000","startDate":"2019-10-01","type":"Entertainment"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"15000","startDate":"2019-10-01","type":"Dressing"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"12000","startDate":"2019-10-01","type":"Leave"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"12000","startDate":"2019-10-01","type":"Utilities"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"13500","startDate":"2019-10-01","type":"Meal"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"22500","startDate":"2019-10-01","type":"Transport"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"22500","startDate":"2019-10-01","type":"Housing"},{"basis":"Monthly","endDate":"2019-10-31","frequency":"Monthly","rate":"18000","startDate":"2019-10-01","type":"Basic"}]


