# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :billout,
  ecto_repos: [Billout.Repo]

# Configures the endpoint
config :billout, BilloutWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Ulx2cwooyK5s9D5L4rWM6YKmVdbOdervmIzSAQwHXyt2EfqWlYCniKGDqrmwssT3",
  render_errors: [view: BilloutWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Billout.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]


config :scrivener_html,
  routes_helper: Billout.Router.Helpers

# If you use a single view style everywhere, you can configure it here. See View Styles below for more info.
# view_style: :bootstrap

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
import_config "smtp.exs"

#Pow Authentication Config
config :billout, :pow,
  user: Billout.Users.User,
  repo: Billout.Repo,
  extensions: [PowResetPassword],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks,
  web_module: BilloutWeb,
  routes_backend: BilloutWeb.Pow.Routes,
  mailer_backend: BilloutWeb.PowMailer,
  web_mailer_module: BilloutWeb


#Bamboo Mailer Config
config :billout, Billout.Mailer,
  adapter: Bamboo.SMTPAdapter,
  server: "smtp.gmail.com",
  port: 465,
  tls: :if_available,
  allowed_tls_versions: [:"tlsv1", :"tlsv1.1", :"tlsv1.2"],
  ssl: true,
  retries: 1,
  no_mx_lookups: false

  config :billout, BilloutWeb.PowMailer,
  adapter: Bamboo.SMTPAdapter,
  server: "smtp.gmail.com",
  port: 465,
  tls: :if_available,
  allowed_tls_versions: [:"tlsv1", :"tlsv1.1", :"tlsv1.2"],
  ssl: true,
  retries: 1,
  no_mx_lookups: false



