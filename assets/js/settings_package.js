import Vue from 'vue'
import axios from 'axios'
import Modal4 from "../components/modal_4.vue"
import modalcreate from "../components/create-package.vue"
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/umd/locale/en'
import locale from 'element-ui/lib/locale'
import VueDataTables from 'vue-data-tables'

locale.use(lang)
Vue.use(ElementUI)
Vue.use(VueDataTables)

var settings = new Vue({
  el: '#settings-package',
  data() {
    return {
      packages: [],
      clickedpackage:'',
      total: 1,
      selectedRow: [],
      pagination: {
        pageSizes: [25, 50, 100]
      },
      tableProps: {
        border: false,
        stripe: true,
        defaultSort: {
          prop: 'flow_no',
          order: 'descending'
        }
      },
      layout: 'pagination, table',
      row_data: '',
      display_show_modal: false,
      display_create_modal: false,
      row_index: "",
      open_modal3: "false",
      basis: ["Daily", "Hourly", "Monthly"],  
      isActive: true,
      id: "",
      earnings: [],
      earningType: ["Basic", "Housing", "Transport", "Meal", "Utilities", "Leave", "Dressing", "Entertainment", "Telephone", "Car", "Benefit" ],
      frequencies: ["Daily", "Weekly", "Monthly", "Fortnightly"],
      packages: [],
      types: ["Basic", "Housing", "Transport", "Meal", "Utilities", "Leave", "Dressing", "Entertainment", "Telephone", "Car", "Benefit" ],
      earning: {
        "type": "Salary",
        "frequency": "Daily",
        "basis": "Monthly",
        "rate": 0
      },
      tag: "",
      description: "",
      alertError: false,
      alertSuccess: false,
      alertMessage: "",
      errorTag: false,
      errorDescription: false
    }
  },
  components: {
    Modal4, modalcreate
  },
  mounted() {
    var self = this
    var self = this;
      axios
      .get("/api/list_packages")
      .then(function(response) {
        self.packages = response.data.packages;
        self.total = self.packages.length
       })
        .catch(function(error) {
        });

        
  },
  methods: {
    changed: function (event) {
      this.$store.commit('change', event.target.value)
    },
    addDetails(event) {
      this.isActive = true
      this.showForm = true
    },
    addPackage() {
      const employee_ids = []
      if (this.selectedRow.length === 0) {
        alert("Dude focus!!! Select something first")
      } else {
        this.selectedRow.forEach((item) => employee_ids.push(item['id']))
        console.log(employee_ids)
      }
    },
    close(event) {
      this.isActive = false
    },
    toggleModal: function (show_modal) {
      this.display_show_modal = show_modal
    },
    toggleModalCreate: function (show_modal) {
      this.display_create_modal = show_modal
    },
    fetchData(query) {
      console.log("Query:", query)
      axios.get('/api/packages', {
          params: {
            page: query.page
          }
        })
        .then((response) => {
          //this.$store.commit('change_employees', response.data.employees)
          //this.employees = this.employee_data
          this.packages =response.data.packages
          this.total = response.data.total
          
        });
    },
    handleSelectionChange(val) {
      this.selectedRow = val
    },
    tableListner(row) {
      if (row.name !== null) {
        return [{
          name: row.name,
          handler: _ => {
            this.clickedpackage = row;
            console.log(this.clickedpackage)
            this.display_show_modal = true
          },
        }]
      }
    },
    addItem(index) {
      this.earnings[index].type = this.earning.type
      this.earnings[index].frequency = this.earning.frequency
      this.earnings[index].basis = this.earning.basis
      this.earnings[index].rate = this.earning.rate
      this.earnings[index].is_editable = false
      
      this.resetEarning()
    },
    addRow() {
      this.earnings.push({
        "type": "Salary",
        "frequency": "Daily",
        "basis": "Monthly",
        "rate": 0,
        "is_editable": true
      })
    },
    addType(event) {
      console.log(event.target.value)
    },
    cancelItem(index) {
      this.$set(this.earnings[index], 'is_editable', false)
    },
    editRow(index) {
      this.$set(this.earnings[index], 'is_editable', true)
      this.earning.type = this.earnings[index].type,
      this.earning.frequency = this.earnings[index].frequency,
      this.earning.basis = this.earnings[index].basis,
      this.earning.rate = this.earnings[index].rate
    },
    deleteRow(index) {
      this.earnings.splice(index, 1)
      console.log(this.earnings)
    },
    resetEarning() {
      this.earning.type = "Salary",
      this.earning.frequency = "Monthly",
      this.earning.basis = "Monthly",
      this.earning.rate = 0
    },
    submit() {
      var url = "/api/package"
      var params = {
        "package":
        {
          "tag": this.tag,
          "description": this.description
        }
      }

      let csrf = document.querySelector("meta[name='csrf-token']").getAttribute("content");
      if(this.tag && this.description != "") {
        axios.post(url, params, {headers: {'Content-Type': 'application/json', 'x-csrf-token': csrf}})
        .then((response) => {
          console.log(response.data.id)

          var urls = "/api/earnings/upsert";
          var paramss = {package: response.data.id, earning: this.earnings};
          
          axios.post(urls, paramss, { headers: {'Content-Type': 'application/json', 'x-csrf-token': csrf}})
          .then(response => {
              console.log(response.data)
          });
        })

        this.alertMessage = "Package Created Successfully"
        this.alertSuccess = true
        this.tag = ""
        this.description = ""
        var self = this;
        setTimeout(function(){self.alertSuccess = false; window.location.reload(); }, 5000);
        
        
      } else {
        var self = this;
        if(self.tag === "") {
          self.errorTag = true
        }
        if (self.description === ""){
          self.errorDescription = true
        }
        this.alertMessage = "Error! Some fields are not filled properly"
        this.alertError = true
        setTimeout(function(){self.alertError = false; }, 5000);
      }

      
    },
    closeAlert() {
      if(this.alertSuccess === true) {
        this.alertSuccess = false
      } else if (this.alertError === true) {
        this.alertError = false
      }
    }
  }
   
})
