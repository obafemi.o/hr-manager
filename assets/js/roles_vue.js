import Vue from 'vue'
import Role from "../components/role-modal.vue"
import store from './store/store'
import { mapGetters,mapState } from 'vuex'

var roles = new Vue({
  el: '#role', 
  store,
  
  data(){
    return {
      display_add_role_modal: false,    
      display_edit_role_modal: false,
      display_add: false,
      display_edit: false,
      role:[],
      checkedPermissions: []
      

    }
  },
  components: {
    Role
  }, 

  computed: {
    ...mapGetters([
      'user_roles'      
    ])

    
  },
  
  updated(){  
    
     },
  methods: {
    open_add_role_modal:function(event){
      this.display_add_role_modal=true;
      this.display_add=true;
      this.display_edit=false;   
    },
    open_edit_role_modal:function(event){
      this.display_edit_role_modal=true;
      this.display_add=false;
      this.display_edit=true;      
    },
    toggleModal:function(){
      this.display_edit_role_modal=false
      this.display_add_role_modal= false    
                                                                                                                    
    },
    close:function (event) {
      this.$emit("close");
    },
    onChange(role) {
      this.checkedPermissions=[];
      console.log(role.permissions.length)
      for (var i=0; i< role.permissions.length; i++){
        this.checkedPermissions.push(role.permissions[i].name)         
      }
  }    
        
  },

  mounted(){    
    const axios = require('axios');
    axios.get('/api/settings/get_role')
    .then((response) => {
      this.$store.commit('list_roles', response.data.roles) 
      
    });

  }
}) 