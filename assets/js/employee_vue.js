import Vue from 'vue'
import ElementUI from 'element-ui'
// import lang from 'element-ui/lib/umd/locale/en'
import locale from 'element-ui/lib/locale'
import VueDataTables from 'vue-data-tables'
import Modal1 from "../components/modal_1.vue"
import Modal2 from "../components/modal_2.vue"
import Modal3 from "../components/modal_3.vue"
import bulkassignrole from "../components/bulk_assign_role.vue"
import axios from 'axios'
import store from './store/store'
import { mapGetters,mapState } from 'vuex'

// locale.use(lang)
Vue.use(ElementUI, { locale })
Vue.use(VueDataTables)

var employee = new Vue({
  el: '#employee',
  store,
  data(){
    return {
      employees: [],
      total: 0,
      selectedRow: [],
      selectionMessage: '',
      pagination: { pageSizes: [25, 50, 100] },
      tableProps: {
      border: false,
      stripe: true,
      defaultSort: {
        prop: 'flow_no',
        order: 'descending'
        }
      },
      layout: 'pagination, table',
      row_data:'',
      display_show_modal:false,
      row_index:"",
      open_modal3:"false",
      display_add_employee_modal: false,
      display_bulk_upload_modal: false,
      display_bulk_assign_role_modal:false
    }
  },
  components: {
    Modal1, Modal2, Modal3, bulkassignrole
  },
 computed: {
    ...mapGetters([
      'employee_data',
      'clicked_employee',
      'saved_package_alert'
    ]),
   selectionExists() {
     return this.selectedRow.length > 0 && this.selectedRow != null
   }
  },
  
  updated(){
    this.employees = this.employee_data;

    this.$store.watch(() =>this.saved_package_alert, saved_package_alert => {
      console.log('watched: ', saved_package_alert)
      if (this.saved_package_alert == true){
        setTimeout (()=> {
          this.$store.commit('toggle_saved_package_alert', false)}, 3000);
      }
    })
  
     },
  methods: {
    changed: function(event) {
      this.$store.commit('change', event.target.value)
    },
    addDetails(event) {
      this.isActive = true
      this.showForm = true
    },
    addPackage() {
      const employee_ids = []
      if (this.selectedRow.length === 0) {
        alert("Dude focus!!! Select something first")
      } else {
        this.selectedRow.forEach((item) => employee_ids.push(item['id']))
       
      }
    },
    close(event) {
      this.isActive = false
    },
    toggleModal:function(){
      this.display_show_modal=false
      this.display_add_employee_modal = false
      this.display_bulk_upload_modal = false
      this.display_bulk_assign_role_modal = false
                                                                                                                    
    },
    open_modal_1:function(event){
      this.display_add_employee_modal =true
      
    },
    open_modal_2:function(event){
      this.display_bulk_upload_modal =true
      
    },
    change_roles:function(event){      
      if (this.selectedRow.length){
      this.display_bulk_assign_role_modal = true
      }
    },
    fetchData(query) {
      axios.get('api/employees', {
        params: {page: query.page, pageSize: query.pageSize}
      })
      .then((response) => {
        this.$store.commit('change_employees', response.data.employees)
        this.employees = this.employee_data
        this.total = response.data.total
      });
    },
    handleSelectionChange(val) {
      this.selectionMessage = (val.length == 1) ? "1 Selected" : val.length + " Selected"
      this.selectedRow = val
      
    },
    toggleSelection() {
      if (this.selectedRow) {
        this.selectedRow.forEach(row => {
          console.log("Mult", this.$refs.multipleTable)
          this.$refs.multipleTable.toggleRowSelection(row);
        });
      } else {
        this.$refs.multipleTable.clearSelection();
      }
    },
    tableListner(row) {
      if (row.name !== null) {
        return [{
          name: row.name,
          handler: _ => {
            this.row_index= this.employee_data.indexOf(row)
            this.$store.commit('change_index', this.row_index)
            this.row_data= this.clicked_employee;
            this.display_show_modal = true
          },
        }]
      }
    },
  }
}) 