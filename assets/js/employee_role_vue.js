import Vue from 'vue'
import employeePayroll from "../components/employee-payroll.vue"
import axios from 'axios'
import store from './store/store'
import {
    mapGetters,
    mapState
} from 'vuex'

var employeeRole = new Vue({
    el: "#employeeRole",
    data() {
        return {
            name: "test",
            employees: [],
            total: 0,
            selectedRow: [],
            selectionMessage: '',
            pagination: {
                pageSizes: [25, 50, 100]
            },
            tableProps: {
                border: false,
                stripe: true,
            defaultSort: {
                prop: 'flow_no',
                order: 'descending'
                }
            },
            layout: 'pagination, table',
            row_data: '',
            display_payslip: false,
            display_show_modal: false,
            row_index: "",
            payrolls: [],
            yearPayrolls: [],
            year: 2019,
            payDefault: true,
            payPrevious: false,
            start_date: "",
            end_date: "",
            payrunIds: [],
            payrunId:"",
            payslip_data: []
        }
    },
    components: {
        employeePayroll
    },
    mounted() {
        var self = this
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        axios.get("/api/user/payroll")
            .then(function (response) {
                console.log(response.data.payroll)
                self.payrolls = response.data.payroll
                for (var i = 0; i < self.payrolls.length; i++) {
                    self.payrolls[i].date = months[new Date(parseInt(self.payrolls[i].payrun_id)).getMonth()]
                    self.payrolls[i].year = new Date(parseInt(self.payrolls[i].payrun_id)).getFullYear()
                }

            })
            .catch(function (error) {
                error
            })

    },
    methods: {
        getPayroll: function () {
            this.yearPayrolls = []
            for (var i = 0; i < this.payrolls.length; i++) {
                if (this.payrolls[i].year === parseInt(this.year)) {
                    this.yearPayrolls.push(this.payrolls[i])
                } else {
                    this.payrolls[i]
                }
            }
            this.payDefault = false
            this.payPrevious = true
        },
        tableListner(row) {
            this.display_payslip = true
            // this.payslip_data = row
        },
        closePayslip(e) {
            this.display_payslip = false
        },
    },
});

// var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];