import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        employees: [],
        index:'',
        earnings: [],
        employees_ledger: [],
        employees_ledger_total: 0,
        saved_package_alert: false,
        roles: []
      },
      mutations: {
        change_employees(state, employees) {
          state.employees = employees
        },
        change_index(state, index) {
          state.index = index
        },
        update_employee(state, employee){
          Vue.set(state.employees, state.index, employee) 
        },
        change_employees_ledger(state, employees_ledger) {
          state.employees_ledger = employees_ledger
        },
        change_employees_ledger_total(state, employees_ledger_total) {
          state.employees_ledger_total = employees_ledger_total
        },
        toggle_saved_package_alert(state, saved_package_alert) {
          state.saved_package_alert = saved_package_alert
        },
        list_roles(state, roles){
          state.roles = roles
        }
        
      },
      getters: {
        employees_ledger( state){return state.employees_ledger},
        employees_ledger_total( state){return state.employees_ledger_total},
        employee_data( state){return state.employees},
        clicked_employee(state){return state.employees[state.index]},
        earnings_data(state){return state.earnings},
        saved_package_alert(state){return state.saved_package_alert},
        user_roles( state){return state.roles}
      },

      
})
