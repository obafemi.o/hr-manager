import Vue from 'vue'
import Payroll from "../components/Payroll.vue"
import Payslip from "../components/payslip.vue"
import PayRegister from "../components/PayRegister.vue"
import PayrollSearch from "../components/PayrollSearch.vue"
import axios from 'axios'
import store from './store/store'
import { mapGetters,mapState } from 'vuex'


var payroll = new Vue({
  el: '#payroll',
  store,
   data(){
    return {
      payroll_data: [],
      total: null,
      pagination: { pageSizes: [25, 50, 100] },
      tableProps: {
      border: false,
      stripe: true,
      defaultSort: {
        prop: 'flow_no',
        order: 'descending'
        }
      },
      layout: 'pagination, table',
      show_modal: false,
      display_payslip: false,
      display_payroll_search: false,
      payslip_data: [],
      display_show_modal: false,
      previousView: false,
      start_date: "",
      end_date: "",
      payrunIds: [],
      payrunId:"",
      payruns: [],
      payrun: "",
      index: true,
      show: false
    }
  },
  components: {
    Payroll, Payslip,
    'payrollsearch': PayrollSearch,
    'payregister' : PayRegister
  },
  computed: {
    ...mapGetters([
      'employees_ledger',
      'employees_ledger_total'
    ]),
    ...mapState(['employees_ledger', 'employees_ledger_total'])
  },
  watch: {
    employees_ledger(newValue) {
      this.payroll_data = newValue
    }, 
    employees_ledger_total(newValue) {
      this.total = newValue
    } 
  },
  mounted() {
    this.listPayruns()
    // this.getPayrun(8)
  },
  methods: {
    listPayruns() {
      axios.get('api/payrun')
      .then((response)=>{
        this.payruns = response.data.payruns
        this.total = response.data.length
        

        for(var i = 0; i < this.payruns.length; i++) {
          let earnings = 0
          let deductions = 0
            if (this.payruns[i].ledgers.length === 0) {
              earnings = earnings + 0
              deductions = deductions + 0

            } else {
              // earnings = earnings + this.payruns[i].ledgers.reduce((a, item) => a + parseFloat(item.value), 0)
              // deductions = deductions + this.payruns[i].ledgers.reduce((a, item) => a + parseFloat(item.value), 0)

             
              for(var j = 0; j < this.payruns[i].ledgers.length; j++) {
                if(this.payruns[i].ledgers[j].earnings === null){
                  earnings = earnings + 0
                } else {
                  earnings = parseInt(this.payruns[i].ledgers[j].earnings) + earnings
                }

                if(this.payruns[i].ledgers[j].deductions === null){
                  deductions =+ 0
                } else {
                  deductions = parseInt(this.payruns[i].ledgers[j].deductions) + deductions
                }
                  
              }
            }
          this.payruns[i].earnings = earnings
          this.payruns[i].deductions = deductions
        }
        
      })
    },
    getPayrun(id) {
      axios.get('api/payrun/'+ id)
      .then((response)=>{
        console.log(response.data.ledgers)
        this.payroll_data = response.data
        this.payslip_data = response.data.payrun

        console.log(this.payroll_data)

        this.index = false
        this.show = true
      })
    },
    showIndex() {
      this.index = true
      this.show = false
    },
    addDetails(event) {
      this.isActive = true
      this.showForm = true
    },
    close(event) {
      this.isActive = false
    },
    getPayrunIds() {
      axios.get('api/ledger/previous', {
        params: {
          start_date: this.start_date,
          end_date: this.end_date
        }
      })
      .then((response) => {
        console.log(response.data.payroll)
        var payrun = response.data.payroll
        var payruns = []
        for(var i = 0; i < payrun.length; i++) {
          payruns.push(payrun[i].payrun_id)
        }
        this.payrunIds = [...new Set(payruns)]
      })
    },
    previousPayrun() {
      axios.get('api/payroll/previous/'+ this.payrunId)
      .then((response) => {
        this.payroll_data = response.data.payroll
      }).catch((err) => {
        
      });
    },
    closePayslip(e) {
      this.display_payslip = false
    },
    closePayrollSearch() {
      this.display_payroll_search = false
    },
    fetchData(query) {
        axios.get('api/payroll', {
        params: {page: query.page, pageSize: query.pageSize}
      })
      .then((response) => {
        this.$store.commit('change_employees_ledger', response.data.payroll)
        this.$store.commit('change_employees_ledger_total', response.data.total)
        this.payroll_data = this.employees_ledger
        this.total=this.employees_ledger_total     
        console.log(response.data.payroll)  
        console.log(response.data.total)
      });
    },
    formatNumber(number, signed = false) {
      let result = new Intl.NumberFormat().format(number)
      return (signed ? `- ₦ ${result}` : `₦ ${result}`)
    },
    showPayrollSearch() {
      this.display_payroll_search = true
    },
    previous() {
      this.previousView = true
    },
    tableListner(row) {
      if (row.name !== null) {
        return [{
          name: row.name,
          handler: _ => {
            this.display_payslip = true
            this.payslip_data = row
          },
        }]
      }
    },
  }
})