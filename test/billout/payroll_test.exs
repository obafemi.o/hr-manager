defmodule Billout.PayrollTest do
  use Billout.DataCase

  alias Billout.Payroll

  describe "ledgers" do
    alias Billout.Payroll.Ledger

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def ledger_fixture(attrs \\ %{}) do
      {:ok, ledger} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Payroll.create_ledger()

      ledger
    end

    test "list_ledgers/0 returns all ledgers" do
      ledger = ledger_fixture()
      assert Payroll.list_ledgers() == [ledger]
    end

    test "get_ledger!/1 returns the ledger with given id" do
      ledger = ledger_fixture()
      assert Payroll.get_ledger!(ledger.id) == ledger
    end

    test "create_ledger/1 with valid data creates a ledger" do
      assert {:ok, %Ledger{} = ledger} = Payroll.create_ledger(@valid_attrs)
    end

    test "create_ledger/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Payroll.create_ledger(@invalid_attrs)
    end

    test "update_ledger/2 with valid data updates the ledger" do
      ledger = ledger_fixture()
      assert {:ok, %Ledger{} = ledger} = Payroll.update_ledger(ledger, @update_attrs)
    end

    test "update_ledger/2 with invalid data returns error changeset" do
      ledger = ledger_fixture()
      assert {:error, %Ecto.Changeset{}} = Payroll.update_ledger(ledger, @invalid_attrs)
      assert ledger == Payroll.get_ledger!(ledger.id)
    end

    test "delete_ledger/1 deletes the ledger" do
      ledger = ledger_fixture()
      assert {:ok, %Ledger{}} = Payroll.delete_ledger(ledger)
      assert_raise Ecto.NoResultsError, fn -> Payroll.get_ledger!(ledger.id) end
    end

    test "change_ledger/1 returns a ledger changeset" do
      ledger = ledger_fixture()
      assert %Ecto.Changeset{} = Payroll.change_ledger(ledger)
    end
  end
end
