defmodule BilloutWeb.EmployeeControllerTest do
  use BilloutWeb.ConnCase

  alias Billout.Organization

  @create_attrs %{
    address: "some address",
    children: 42,
    country: "some country",
    dependents: 42,
    dob: ~D[2010-04-17],
    email: "some email",
    first_name: "some first_name",
    marital_status: "some marital_status",
    middle_name: "some middle_name",
    phone: "some phone",
    state: "some state",
    surname: "some surname",
    title: "some title"
  }
  @update_attrs %{
    address: "some updated address",
    children: 43,
    country: "some updated country",
    dependents: 43,
    dob: ~D[2011-05-18],
    email: "some updated email",
    first_name: "some updated first_name",
    marital_status: "some updated marital_status",
    middle_name: "some updated middle_name",
    phone: "some updated phone",
    state: "some updated state",
    surname: "some updated surname",
    title: "some updated title"
  }
  @invalid_attrs %{
    address: nil,
    children: nil,
    country: nil,
    dependents: nil,
    dob: nil,
    email: nil,
    first_name: nil,
    marital_status: nil,
    middle_name: nil,
    phone: nil,
    state: nil,
    surname: nil,
    title: nil
  }

  def fixture(:employee) do
    {:ok, employee} = Organization.create_employee(@create_attrs)
    employee
  end

  describe "index" do
    test "lists all employees", %{conn: conn} do
      conn = get(conn, Routes.employee_path(conn, :index))
      assert html_response(conn, 200) =~ "All Employees"
    end
  end

  describe "new employee" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.employee_path(conn, :new))
      assert html_response(conn, 200) =~ "New Employee"
    end
  end

  describe "create employee" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.employee_path(conn, :create), employee: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.employee_path(conn, :show, id)

      conn = get(conn, Routes.employee_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Employee"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.employee_path(conn, :create), employee: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Employee"
    end
  end

  describe "edit employee" do
    setup [:create_employee]

    test "renders form for editing chosen employee", %{conn: conn, employee: employee} do
      conn = get(conn, Routes.employee_path(conn, :edit, employee))
      assert html_response(conn, 200) =~ "Edit Employee"
    end
  end

  describe "update employee" do
    setup [:create_employee]

    test "redirects when data is valid", %{conn: conn, employee: employee} do
      conn = put(conn, Routes.employee_path(conn, :update, employee), employee: @update_attrs)
      assert redirected_to(conn) == Routes.employee_path(conn, :show, employee)

      conn = get(conn, Routes.employee_path(conn, :show, employee))
      assert html_response(conn, 200) =~ "some updated address"
    end

    test "renders errors when data is invalid", %{conn: conn, employee: employee} do
      conn = put(conn, Routes.employee_path(conn, :update, employee), employee: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Employee"
    end
  end

  describe "delete employee" do
    setup [:create_employee]

    test "deletes chosen employee", %{conn: conn, employee: employee} do
      conn = delete(conn, Routes.employee_path(conn, :delete, employee))
      assert redirected_to(conn) == Routes.employee_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.employee_path(conn, :show, employee))
      end
    end
  end

  describe "download sample file" do
    test "get file", %{conn: conn} do
      # conn = get(conn, Routes.employee_path(conn, :download_sample))
      file = File.read!("priv/static/samples/sample.csv")
      response = conn |> get(Routes.employee_path(conn, :download_sample))
      assert file == response.resp_body
    end
  end

  defp create_employee(_) do
    employee = fixture(:employee)
    {:ok, employee: employee}
  end
end
