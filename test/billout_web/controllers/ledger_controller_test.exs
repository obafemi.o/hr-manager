defmodule BilloutWeb.LedgerControllerTest do
  use BilloutWeb.ConnCase

  alias Billout.Payroll

  @create_attrs %{
    payrun_id: "20190703151345",
    start_date: "2019-06-03",
    end_date: "2019-07-03"
  }

  @invalid_attrs %{
    payrun_id: nil,
    start_date: nil,
    end_date: nil
  }

  describe "create ledger" do
    test "confirm success message", %{conn: conn} do
      conn = post(conn, Routes.api_ledger_path(conn, :create), ledger_params: @create_attrs)

      # assert conn.resp_body.message == "Success"
      assert conn.status == 201
    end

    test "confirm error message", %{conn: conn} do
      conn = post(conn, Routes.api_ledger_path(conn, :create), ledger_params: @invalid_attrs)

      IO.inspect conn.status
      assert conn.status == 500
    end
  end

end
